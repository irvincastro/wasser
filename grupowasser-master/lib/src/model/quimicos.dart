
class Quimicos{
  int id;
  String nombre;
  bool mostraInfo = false;
  double cantidad;
  String unidad;
  String dosificacion;
  Quimicos({
    this.cantidad,this.dosificacion,this.nombre,this.unidad,this.id
  });
}

List<Quimicos> quimico = [
  Quimicos(
    id: 1,
    nombre: "Algen",
    cantidad: 0,
    unidad: "ML",
    dosificacion: "1 Lts* cada 50 mts3",
  ),
  Quimicos(
     id: 2,
    nombre: "Clarificador",
    cantidad: 0,
    unidad: "ML",
    dosificacion: "3 Mts* cada mts3",
  ),
  Quimicos(
     id: 3,
    nombre: "Kristal",
    cantidad: 0,
    unidad: "ML",
    dosificacion: "5 Lts* cada mts3",
  ),
  Quimicos(
     id: 4,
    nombre: "Hipoclorito de sodio",
    cantidad: 0,
    unidad: "Grs",
    dosificacion: "13 gramos* cada mts3",
  ),
  Quimicos(
     id: 5,
    nombre: "Acido Clorhidrico",
    cantidad: 0,
    unidad: "Grs",
    dosificacion: "10 gramos* cada mts3",
  ),
  Quimicos(
     id: 6,
    nombre: "PH+",
    cantidad: 0,
    unidad: "Grs",
    dosificacion: "10 gramos* cada mts3",
  ),
  Quimicos(
     id: 7,
    nombre: "Ph-",
    cantidad: 0,
    unidad: "Grs",
    dosificacion: "10 Lts* cada mts3",
  ),
  Quimicos(
     id: 8,
    nombre: "Cloro",
    cantidad: 0,
    unidad: "Grs",
    dosificacion: "1.3 Grs* cada mts3",
  ),
  Quimicos(
    id: 9,
    nombre: "Shock",
    cantidad: 0,
    unidad: "Grs",
    dosificacion: "20 gramos * cada mts3",
  ),
];