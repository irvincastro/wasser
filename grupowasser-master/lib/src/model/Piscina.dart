class Piscina {
//los atributos del cliente
  int id;
  String nombre;
  double metroCubico;
  int tipoAlberca;
  String filtrado;
  String bomba;
  String correo;
  Piscina({ this.filtrado, this.metroCubico,
      this.tipoAlberca, this.nombre,this.id,this.bomba,this.correo});

//para poder insertar en la base de datos necesitamos crearnos un map
  Map<String, dynamic> toMap() {
    return {
      "id": id,
      "metroCubico": metroCubico,
      "tipoALberca": tipoAlberca,
      "filtrado": filtrado,
      "nombre": nombre,
      "bomba":bomba,
      "correo":correo
      
    };
  }

  Piscina.fromMap(Map<String, dynamic> map) {
    id = map["id"];
    metroCubico = map["metroCubico"];
    tipoAlberca = map["tipoALberca"];
    filtrado = map["filtrado"];
    nombre = map["nombre"];
    bomba = map["bomba"];
    correo = map["correo"];
  }
}
