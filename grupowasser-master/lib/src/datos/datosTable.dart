import 'package:flutter/material.dart';
import 'package:responsive_table/responsive_table.dart';

List<DatatableHeader> header = [
  DatatableHeader(
      text: "Quimico",
      value: "Quimico",
      show: true,
      flex: 2,
      sortable: true,
      textAlign: TextAlign.right),
  DatatableHeader(
      text: "Cantidad",
      value: "Cantidad",
      show: true,
      flex: 2,
      sortable: true,
      textAlign: TextAlign.left),
  DatatableHeader(
      text: "Unidad",
      value: "Unidad",
      show: true,
      flex: 2,
      sortable: true,
      textAlign: TextAlign.center),
  DatatableHeader(
      text: "Dosificación x M3",
      value: "Dosificación x M3",
      show: true,
      flex: 2,
      sortable: true,
      textAlign: TextAlign.left),
];
