import 'dart:io';
import 'package:grupo_wasser/src/model/Piscina.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class TaskDatabase {
  Database _database; //instanciamos el data base para usarlo
  String basededatos = "Piscina.db";
  String tabla = "Piscina";
  TaskDatabase._();
  static final TaskDatabase db = TaskDatabase._();

  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await getDatabaseInstanace();
    return _database;
  }

  Future<Database> getDatabaseInstanace() async {
    Directory directory = await getApplicationDocumentsDirectory();
    String path = join(
        directory.path, basededatos); // le asignamos nombre ala base de datos
    return await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
      await db.execute("CREATE TABLE " +
          tabla +
          " (" //ejecutamos el query para crear la tabla con sus atributos
              "id integer primary key,"
              "nombre TEXT,"
              "metroCubico REAL,"
              "tipoALberca INTEGER,"
              "filtrado TEXT,"
              "bomba TEXT,"
              "correo TEXT"
              ")");
               
    });
  }

  Future<List<Piscina>> getAllPiscina() async {
    final db = await database;
    var response = await db.query(tabla);
    List<Piscina> list = response.map((e) => Piscina.fromMap(e)).toList();
    return list;
  }

  Future<Piscina> getPiscinaWithId(int id) async {
    final db = await database;
    var response = await db.query(tabla, where: "id = ?", whereArgs: [id]);
    return response.isNotEmpty ? Piscina.fromMap(response.first) : null;
  }

  Future<List<Piscina>> getallPiscinaUserCorreo(String correo) async {
    final db = await database;
    var response = await db.query(tabla, where: "correo = ?", whereArgs: [correo]);
    
    List<Piscina> list = response.map((e) => Piscina.fromMap(e)).toList();
    return list;
  }

  addPiscinaToDatabase(Piscina piscina) async {
    final db = await database;
    var table = await db.rawQuery("SELECT MAX(id)+1 as id FROM " + tabla);
    int id = table.first["id"];
    piscina.id = id;
    var raw = await db.insert(
      tabla,
      piscina.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    return raw;
  }

  deletePiscinaWithId(int id) async {
    final db = await database;
    return db.delete(tabla, where: "id = ?", whereArgs: [id]);
  }

  deleteAllPiscina() async {
    final db = await database;
    db.delete(tabla);
  }

  updatePisicna(Piscina piscina) async {
    final db = await database;
    var response = await db.update(tabla, piscina.toMap(),
        where: "id = ?", whereArgs: [piscina.id]);
    return response;
  }
}
