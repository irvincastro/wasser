import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:grupo_wasser/main.dart';
import 'package:grupo_wasser/src/model/bomba_modelo.dart';
import 'package:grupo_wasser/src/model/filtro_model.dart';

class UserRepository {
  final FirebaseAuth firebaseAuth;
  // final Google googleSingIn;

  UserRepository({FirebaseAuth firebaseAuth})
      : firebaseAuth = firebaseAuth ?? FirebaseAuth.instance;

  Future<void> signInWithCredentials(String correo, String password) {
    return firebaseAuth.signInWithEmailAndPassword(
        email: correo, password: password);
  }

  Future<void> signOut() async {
    return Future.wait([firebaseAuth.signOut()]);
  }

  Future<void> registro(String correo, String passwor) async {
    return await firebaseAuth.createUserWithEmailAndPassword(
        email: correo, password: passwor);
  }

  Future<String> getUser() async {
    return (await firebaseAuth.currentUser).email;
  }
}

Future<DocumentSnapshot> getUser() {
  Future<DocumentSnapshot> user = FirebaseFirestore.instance
      .collection('usaurios')
      .doc("${prefs.correo}")
      .get();
  
  return user;
}

Future<List<Filtro>>  getFiltro()async{
    List<Map<String, dynamic>> lista = List<Map<String, dynamic>>();
    List<DocumentChange> documenots = List<DocumentChange>();
    await FirebaseFirestore.instance.collection('filtro').get().then((value){
       documenots = value.docChanges.toList();
       for (var item in documenots) {
          lista.add(item.doc.data());
       }

   });
   return lista.map((e) => Filtro.fromMap(e)).toList();
 
}
Future<List<Bomba>> getBomba()async{
    List<Map<String, dynamic>> lista = List<Map<String, dynamic>>();
    List<DocumentChange> documenots = List<DocumentChange>();
    await FirebaseFirestore.instance.collection('bomba').get().then((value){
       documenots = value.docChanges.toList();
       for (var item in documenots) {
          lista.add(item.doc.data());
       }

   });
   return lista.map((e) => Bomba.fromMap(e)).toList();
 
}
