import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

//estas son la preferencias que se guardan en cache
class PreferenciasUsuario {
  static final PreferenciasUsuario _instancia =
      new PreferenciasUsuario._internal();

  factory PreferenciasUsuario() {
    return _instancia;
  }

  PreferenciasUsuario._internal();

  SharedPreferences prefs;

  initPrefs() async {
    WidgetsFlutterBinding.ensureInitialized();
    this.prefs = await SharedPreferences.getInstance();
  }
 

  
get activo {
    return prefs.getBool('activo') ?? false;
  }

  set activo(bool value) {
    prefs.setBool('activo', value);
  }


  get correo {
    return prefs.getString('correo') ?? '';
  }

  set correo(String value) {
    prefs.setString('correo', value);
  }

  get nombreUsuario {
    return prefs.getString('nombreUsuario') ?? '';
  }

  set nombreUsuario(String value) {
    prefs.setString('nombreUsuario', value);
  }

  get tipo {
    return prefs.getInt('tipo');
  }

  set tipo(int value) {
    prefs.setInt('tipo', value);
  }

  set id(int value) {
    prefs.setInt('id', value);
  }

  get id {
    return prefs.getInt('id');
  }

  

  Future<bool> clear() async {
    var r = await prefs.clear();
    prefs.setInt('calibracionMensaje', 1);
    return r;
  }

  //variables para la introduccion
  get verIntroduccion {
    return this.prefs.getBool('verIntroduccion') ?? false;
  }

  set verIntroduccion(bool ver) {
    this.prefs.setBool('verIntroduccion', ver);
  }
}
