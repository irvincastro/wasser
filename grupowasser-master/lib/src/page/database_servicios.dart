import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:grupo_wasser/src/page/servicios.dart';

class DatabaseHelper {
  static const _databaseName = 'servicios.db';
  static const _databaseVersion = 1;

  //Instancia única de esta clase
  DatabaseHelper._();
  static final DatabaseHelper instance = DatabaseHelper._();

  Database _database;
  Future<Database> get database async{ //Obtener db
    if(_database != null) return _database; //Si es diferente de null porque ya se ha inicializado, retornar db
    _database = await _initDatabase(); //Método para inicializar db
    return _database; //Retorna el objeto db
  }

  _initDatabase()async{ //Future, se usa async y await.
    Directory dataDirectory = await getApplicationDocumentsDirectory(); //Directorio donde se guardará la bd
    String dbPath = join(dataDirectory.path, _databaseName); //Una la ruta con el nombre de la bd en un string
    return await openDatabase(
      dbPath, 
      version: _databaseVersion,
      onCreate: _onCreateDB
    );
  }

  //Crea la estructura de la bd
  _onCreateDB(Database db, int version) async{
    await db.execute('''
      CREATE TABLE ${ServiciosModeloDB.tblServicios}(
        ${ServiciosModeloDB.colId} INTEGER PRIMARY KEY AUTOINCREMENT,
        ${ServiciosModeloDB.colServicio} TEXT NOT NULL,
        ${ServiciosModeloDB.colAlberquero} TEXT NOT NULL,
        ${ServiciosModeloDB.colFecha} TEXT NOT NULL,
        ${ServiciosModeloDB.colEstado} TEXT NOT NULL,
        ${ServiciosModeloDB.colCalificacion} INTEGER
      )
    ''');
  }

  Future<int> insertContact(ServiciosModeloDB service) async{
    Database db = await database;
    return await db.insert(ServiciosModeloDB.tblServicios, service.toMap());
  }

  Future<int> updateContact(ServiciosModeloDB service) async{
    Database db = await database;
    return await db.update(ServiciosModeloDB.tblServicios, service.toMap(),
    where: '${ServiciosModeloDB.colId}=?', whereArgs: [service.id] );
  }

  Future<int> deleteContact(int id) async{
    Database db = await database;
    return await db.delete(ServiciosModeloDB.tblServicios,
    where: '${ServiciosModeloDB.colId}=?', whereArgs: [id] );
  }

  Future<List<ServiciosModeloDB>> fetchContacts() async{
    Database db = await database;
    List<Map> contacts = await db.query(ServiciosModeloDB.tblServicios);
    return contacts.length == 0
    ?
    []
    :contacts.map((e) => ServiciosModeloDB.fromMap(e)).toList();
  }

  //2 MÉTODOS MÁS
  Future<List<Map>> fetchContactsMap() async{
    Database db = await database;
    List<Map> contacts = await db.query(ServiciosModeloDB.tblServicios);
    return contacts.length == 0
    ?
    []
    //:contacts.map((e) => Contact.fromMap(e)).toList();
    :contacts;
  }

  Future<int> deleteContactByName(String name) async{
    Database db = await database;
    return await db.delete(ServiciosModeloDB.tblServicios,
    where: '${ServiciosModeloDB.colAlberquero}=?', whereArgs: [name] );
  }

}