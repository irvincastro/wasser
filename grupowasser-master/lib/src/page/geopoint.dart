/*
*     Objeto utilizado para la gestion de los marcadores
*/

// ------------------------------|>| Importaciones |<|------------------------------
import 'package:cloud_firestore/cloud_firestore.dart';

// ------------------------------|>| Objeto |<|------------------------------
class Geopoint {
  final String name;
  final String lastName1;
  final String lastName2;
  final String phonenumber;
  final GeoPoint location;
  final bool availability;
  final int rating;

  Geopoint(
      {this.name,
      this.lastName1,
      this.lastName2,
      this.phonenumber,
      this.location,
      this.availability,
      this.rating});
}
