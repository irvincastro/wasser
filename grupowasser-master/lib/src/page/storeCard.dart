/*
*     Vista que se despliega al seleccionar un marcador
*/

// ------------------------------|>| Importaciones |<|------------------------------
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;

// ------------------------------|>| Constructor |<|------------------------------
class StoreCard extends StatefulWidget {
  final String name;
  final String lastName1;
  final String lastName2;
  final String phonenumber;
  final GeoPoint location;
  final int rating;

  const StoreCard(
      {Key key,
      this.name,
      this.lastName1,
      this.lastName2,
      this.phonenumber,
      this.location,
      this.rating})
      : super(key: key);
  @override
  _StoreCardState createState() => _StoreCardState();
}

class _StoreCardState extends State<StoreCard> {
//------------|Dialog Box|------------
//------------|Dialog Box|------------

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Text(
          "${widget.name} ${widget.lastName1} ${widget.lastName2}",
          style: TextStyle(fontSize: 24.0),
        ),
        SizedBox(
          height: 20.0,
        ),
        RatingBar(
          initialRating: double.parse(widget.rating.toString()),
          minRating: 1,
          ignoreGestures: true,
          direction: Axis.horizontal,
          allowHalfRating: false,
          itemCount: 5,
          itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
          itemBuilder: (context, _) => Icon(
            Icons.star,
            color: Colors.amber,
          ),
          onRatingUpdate: (rating) {},
        ),
        Text(
          widget.rating.toString(),
          style: TextStyle(fontSize: 18.0),
        ),
        Spacer(),
        Wrap(
          children: <Widget>[
            SizedBox(
              height: 50.0,
              width: 120.0,
              child: RaisedButton(
                  color: Colors.green,
                  child: Text(
                    "Chat",
                    style: TextStyle(fontSize: 25.0),
                  ),
                  onPressed: () {}),
            ),
            SizedBox(
              width: 20.0,
            ),
            SizedBox(
              height: 50.0,
              width: 120.0,
              child: RaisedButton(
                  color: Colors.green,
                  child: Text(
                    "Llamar",
                    style: TextStyle(fontSize: 25.0),
                  ),
                  onPressed: () {
//------------------------------------|Dialog Box|------------------------------------
                    showDialog(
                        context: context,
                        builder: (BuildContext context) => Dialog(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(
                                      12.0)), //this right here
                              child: Container(
                                height: 300.0,
                                width: 300.0,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Padding(
                                      padding: EdgeInsets.all(15.0),
                                      child: Text(
                                        "¿Desea llamar a?",
                                        style: TextStyle(fontSize: 24.0),
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.all(15.0),
                                      child: Text(
                                        "${widget.name} ${widget.lastName1} ${widget.lastName2}",
                                        style: TextStyle(fontSize: 24.0),
                                      ),
                                    ),
                                    Padding(
                                        padding: EdgeInsets.only(top: 30.0)),
                                    Wrap(children: <Widget>[
                                      SizedBox(
                                        height: 50.0,
                                        width: 120.0,
                                        child: FlatButton(
                                            color: Colors.green,
                                            child: Text(
                                              "Cancelar",
                                              style: TextStyle(fontSize: 18.0),
                                            ),
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            }),
                                      ),
                                      SizedBox(
                                        width: 20.0,
                                      ),
                                      SizedBox(
                                        height: 50.0,
                                        width: 120.0,
                                        child: FlatButton(
                                            color: Colors.green,
                                            child: Text(
                                              "Confirmar",
                                              style: TextStyle(fontSize: 18.0),
                                            ),
                                            onPressed: () {
                                              UrlLauncher.launch(
                                                  "tel://${widget.phonenumber}");
                                            }),
                                      ),
                                    ]),
                                  ],
                                ),
                              ),
                            ));
//------------------------------------|Dialog Box|------------------------------------
                  }),
            ),
          ],
        ),
      ],
    );
  }
}
