import 'package:cloud_firestore/cloud_firestore.dart';
import 'geopoint.dart';

class GeoDatabaseService {
// ------------------------------|>| Variables |<|------------------------------
  // Instancia de firebase que obtiene la colecion llamada alberqueros
  CollectionReference geopointCollection =
      FirebaseFirestore.instance.collection('usaurios');

  // Obtenemos todos los datos de la coleccion especificada en la instancia
  List<Geopoint> _geopointListFromSnapshot(QuerySnapshot snapshot) {
    return snapshot.docs.map((document) {
      return Geopoint(
        name: document.data()['nombre'] ?? "NameNotFound",
        lastName1: document.data()['PrimerApellido'] ?? "lastNameNotFound",
        lastName2: document.data()['segundoApellido'] ?? "lastNameNotFound",
        location: document.data()['Location'] ?? GeoPoint(0.0, 0.0),
        availability: document.data()['Availability'] ?? false,
        rating: document.data()['rating'] ?? 0,
        phonenumber: document.data()['phonenumber'] ?? "0",
      );
    }).toList();
  }

  // Creamos un stream de datos que distribuya la informacion obtenida de firebase
  Stream<List<Geopoint>> get markers {
    return geopointCollection
        .where("tipo", isEqualTo: 1)
        .snapshots()
        .map(_geopointListFromSnapshot);
  }
}
