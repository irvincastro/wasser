import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:grupo_wasser/src/widget/menu.dart';
import 'package:grupo_wasser/src/page/database_servicios.dart';
import 'package:intl/intl.dart';

final Color wasserColor = Color.fromRGBO(17, 167, 174, 1);
ServiciosModeloDB serviceModel = ServiciosModeloDB();
List<ServiciosModeloDB> listaServices = [];


class Servicios extends StatefulWidget {
  final Color wasserColor = Color.fromRGBO(17, 167, 174, 1);
  @override
  _ServiciosState createState() => _ServiciosState();
}

class _ServiciosState extends State<Servicios> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: widget.wasserColor,
          title: Text("Servicios"),
          centerTitle: true,
        ),
        drawer: MenuPage(),
        body: TabBarView(
          children: <Widget>[
            List1(),
            Lista2(),
            Lista3()
          ],
        ),
        bottomNavigationBar: Material(
          //color: widget.wasserColor,
          child: TabBar(
            tabs: <Widget>[
              Tab(
                child: Text(
                  "Mi lista", 
                  style: TextStyle(
                    color: widget.wasserColor
                  ),
                ),
                icon: Icon(
                  Icons.card_travel, 
                  color: widget.wasserColor,
                  size: 30
                )
              ),
              Tab(
                child: Text(
                  "Alberqueros", 
                  style: TextStyle(
                    color: widget.wasserColor
                  ),
                ),
                icon: Icon(
                  Icons.contacts, 
                  color: widget.wasserColor,
                  size: 30
                )
              ),
              Tab(
                child: Text(
                  "Populares", 
                  style: TextStyle(
                    color: widget.wasserColor
                  ),
                ),
                icon: Icon(
                  Icons.star, 
                  color: widget.wasserColor,
                  size: 30
                )
              ),
              
            ]
          ),
        ),
      ),
    );
  }
}

class ServiciosModel{
  String servicio;
  String descripcion;
  int costo;

  ServiciosModel({this.servicio, this.descripcion, this.costo});

  ServiciosModel.fromMap(Map<String, dynamic> map){
    servicio = map['servicio'];
    descripcion = map['descripcion'];
    costo = map['costo'];
  }

  Map<String, dynamic> toMap(){
    var map = <String, dynamic>{
      'servicio': servicio,
      'descripcion': descripcion,
      'costo': costo,
    };
    return map;
  }
}

class ElegirServicio extends StatefulWidget {
  @override
  _ElegirServicioState createState() => _ElegirServicioState();
}

class _ElegirServicioState extends State<ElegirServicio> {

  DatabaseHelper _dbHelper;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      _dbHelper = DatabaseHelper.instance;
    });
  }

  void showAlert(String servicio, String alberquero, int costo){
    var now = new DateTime.now().add(Duration(days: 1));
    var formatter = new DateFormat('yyyyMMddHH');
    String dateNow = formatter.format(now);
    //print(dateNow);

    AlertDialog dialog = AlertDialog(
      title: Center(
        child: Text("¿Desea agregar este servicio?"),
      ),
      content: StatefulBuilder(
        builder: (context, StateSetter setState){
          return Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                servicio,
                style: TextStyle(
                  fontWeight: FontWeight.bold
                ),
              ),
              Text(
                alberquero,
                style: TextStyle(
                  fontSize: 16,
                  color: Colors.grey
                ),
              ),
              SizedBox(height: 10),
              Text(
                "\$${costo.toString()}.00",
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold
                ),
              ),
            ],
          );
        }
      ),
      actions: <Widget>[
        FlatButton(
          child: Text("Si"), 
          onPressed: ()async{
            await _dbHelper.insertContact(ServiciosModeloDB(
              //id: 0,
              servicio: servicio,
              alberquero: alberquero,
              fecha: dateNow,
              estado: "Pendiente",
              calificacion: 0
            ));
            Navigator.pop(context);
            Navigator.pushReplacement(
              context, 
              MaterialPageRoute(builder: (_) => Servicios())
            );
          },
        ),
        FlatButton(child: Text("No"), onPressed: (){
          Navigator.pop(context);
        }),
      ]
    );
    showDialog(context: context, builder: (BuildContext context) {return dialog;});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Servicios disponibles"),
      ),
      // StreamBuilder(
      //   stream: FirebaseFirestore.instance.collection('serviciosCalificados').snapshots(),
      //   builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot){
      //     if (!snapshot.hasData) return Center(child: CircularProgressIndicator());
      body: StreamBuilder(
        stream: FirebaseFirestore.instance.collection("servicios").snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot){
          if (!snapshot.hasData) return Center(child: CircularProgressIndicator());
          // tam = snapshot.data.docs.length;
          // for (var i = 0; i < tam; i++) {
          //   least.add(snapshot.data.docs[i].data());
          // }
          int tam;
          tam = snapshot.data.docs.length;
          // print(tam);
          // for (var i = 0; i < tam; i++) {
          //   print(snapshot.data.docs[i].data());
          // }
          //print(snapshot.data.docs[0].data()["alberquero"]);
          //String nom
          return SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Container(
                  height: 50,
                  //color: Colors.green,
                  child: Center(
                    child: Text(
                      "Seleccione un servicio", 
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20
                      ),
                    ),
                  ),
                ),
                ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: tam,
                  itemBuilder: (context, index){
                    //return Text("$index");
                    //print("C"+index.toString());
                    return Card(
                      child: ListTile(
                        //leading: Text("${snapshot.data.docs[index].data()["alberquero"]}"),
                        title: Text("${snapshot.data.docs[index].data()["servicio"]}"),
                        subtitle: Text("${snapshot.data.docs[index].data()["descripcion"]}\n${snapshot.data.docs[index].data()["alberquero"]}"),
                        trailing: Text(
                          "\$${snapshot.data.docs[index].data()["costo"]}",
                          style: TextStyle(
                            fontWeight: FontWeight.bold
                          ),
                        ),
                        onTap: (){
                          showAlert(
                            snapshot.data.docs[index].data()["servicio"],
                            snapshot.data.docs[index].data()["alberquero"],
                            snapshot.data.docs[index].data()["costo"]
                          );
                        }
                      ),
                    );
                  }
                )
              ],
            ),
          );
        },
      )
    );
  }
}

class Ordenamiento {
  int id;
  String ordenamiento;

  Ordenamiento({this.id, this.ordenamiento});

  static List<Ordenamiento> getordenamientos(){
    return <Ordenamiento>[
      Ordenamiento(id: 0, ordenamiento: "Reciente"),
      Ordenamiento(id: 1, ordenamiento: "Antiguo"),
      Ordenamiento(id: 2, ordenamiento: "Alberquero"),
      Ordenamiento(id: 3, ordenamiento: "Calificación"),
      Ordenamiento(id: 4, ordenamiento: "Servicio"),
    ];
  }
}

class List1 extends StatefulWidget {
  @override
  _List1State createState() => _List1State();
}

class _List1State extends State<List1> {

  Timer _timer;
  String _timeString;
  bool editable = true;
  String fechaActual;
  int fechaDB;
  int fechaAhora;

  DatabaseHelper _dbHelper;

  List<Ordenamiento> _ordenamientos = Ordenamiento.getordenamientos();
  List<DropdownMenuItem<Ordenamiento>> _dropdownMenuItems;
  Ordenamiento _ordenamientoSeleccionado;

  List<ServiciosModeloDB> listaOrdenada;

    @override
  void initState() {
    // TODO: implement initState
    
    super.initState();
    setState(() {
      _dbHelper = DatabaseHelper.instance;
    });
    listaOrdenada = listaServices;
    actualizarLista();
    print("init");

    _timeString = _formatDateTime(DateTime.now());
    fechaActual = _formatDateTime(DateTime.now());
    print(fechaActual);

    _timer = Timer.periodic(Duration(hours: 1), (Timer t) => _getTime() );

    _dropdownMenuItems = buildDropDownMenuItems(_ordenamientos);
    _ordenamientoSeleccionado = _dropdownMenuItems[0].value;

  }

  List<DropdownMenuItem<Ordenamiento>> buildDropDownMenuItems(List ordenamientos){
    List<DropdownMenuItem<Ordenamiento>> items = List();
    for (Ordenamiento ordenamiento in ordenamientos) {
      items.add(
        DropdownMenuItem(
          value: ordenamiento,
          child: Text(ordenamiento.ordenamiento)
        )
      );
    }
    return items;
  }

  onChangeDropdownItem(Ordenamiento seleccionado){
    setState(() {
      _ordenamientoSeleccionado = seleccionado;
    });
  }



  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _timer.cancel();
  }

  void _getTime(){
    final DateTime now = DateTime.now().add(Duration(days: 1));
    final String formatedDateTime = _formatDateTime(now);

    setState(() {
      _timeString = formatedDateTime;
      print(_timeString);
    });
  }

  String _formatDateTime(DateTime dateTime){
    return DateFormat('yyyyMMddHH').format(dateTime);
  }
  

  Icon estrella(){
    return Icon(
      Icons.star,
      color: Colors.orangeAccent,
      size: 50,
    );
  }

  Icon estrellaBorde(){
    return Icon(
      Icons.star_border,
      color: Colors.orangeAccent,
      size: 50,
    );
  }

  void mostrarEstrellas(int index){
    int id = listaServices[index].id;
    String estado = listaServices[index].estado;
    int calificacion = listaServices[index].calificacion;
    int califTemp = calificacion;
    String fecha = listaServices[index].fecha;
    String fechaLegible = "";

    fechaLegible += fecha.substring(6,8);
    fechaLegible += "/";
    fechaLegible += fecha.substring(4,6);
    fechaLegible += "/";
    fechaLegible += fecha.substring(0,4);

    AlertDialog dialog = AlertDialog(
      title: Center(
        child: Text("Calificación"),
      ),
      content: StatefulBuilder(
        builder: (context, StateSetter setState){
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Center(
                child: Text(
                  "Fecha: $fechaLegible",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 15
                  ),  
                ),
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  //ESTRELLA 1
                  Container(
                    child: califTemp>=1 ? estrella() : estrellaBorde(),
                  ),
                  //ESTRELLA 2
                  Container(
                    child: califTemp>=2 ? estrella() : estrellaBorde(),
                  ),
                  //ESTRELLA 3
                  Container(
                    child: califTemp>=3 ? estrella() : estrellaBorde(),
                  ),
                  //ESTRELLA 4
                  Container(
                    child: califTemp>=4 ? estrella() : estrellaBorde(),
                  ),
                  //ESTRELLA 5
                  Container(
                    child: califTemp==5 ? estrella() : estrellaBorde(),
                  )
                ],
              ),
            ],
          );
        }
      ),
      actions: <Widget>[
        FlatButton(
          child: Text(
            "Eliminar de mi lista",
            style: TextStyle(
              color: Colors.red
            ),
          ), 
          onPressed: ()async{
            await _dbHelper.deleteContact(id);
            actualizarLista();
            Navigator.pop(context);
          }
        ),
        FlatButton(child: Text("Cancelar"), onPressed: (){
          Navigator.pop(context);
        }),
      ]
    );
    showDialog(context: context, builder: (BuildContext context) {return dialog;});
  }

  void showAlert(int index){
    int id = listaOrdenada[index].id;
    String estado = listaOrdenada[index].estado;
    int calificacion = listaOrdenada[index].calificacion;
    int califTemp = calificacion;
    String fecha = listaOrdenada[index].fecha;
    String fechaLegible = "";
    String servicio = listaOrdenada[index].servicio;
    String alberquero = listaOrdenada[index].alberquero;

    fechaLegible += fecha.substring(6,8);
    fechaLegible += "/";
    fechaLegible += fecha.substring(4,6);
    fechaLegible += "/";
    fechaLegible += fecha.substring(0,4);
    fechaLegible += " a las ";
    fechaLegible += fecha.substring(8);
    fechaLegible += " horas";
    //print(fecha.length);

    AlertDialog dialog = AlertDialog(
      title: Center(
        child: estado.contains("Calificado")?
          Text("Modificar calificacion")
          :Text("Calificar servicio"),
      ),
      content: StatefulBuilder(
        builder: (context, StateSetter setState){
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Center(
                child: Text(
                  estado.contains("Calificado")?
                  "Editable hasta: $fechaLegible"
                  :"", 
                  style: TextStyle(
                    color: Colors.red[800],
                    fontSize: 15
                  ),  
                ),
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  //ESTRELLA 1
                  GestureDetector(
                    child: califTemp>=1 ? estrella() : estrellaBorde(),
                    onTap: (){
                      //print("Estrella 1");
                      setState(() {
                        califTemp = 1;
                      });
                    },
                  ),
                  //ESTRELLA 2
                  GestureDetector(
                    child: califTemp>=2 ? estrella() : estrellaBorde(),
                    onTap: (){
                      //print("Estrella 2");
                      setState(() {
                        califTemp = 2;
                      });
                    }
                  ),
                  //ESTRELLA 3
                  GestureDetector(
                    child: califTemp>=3 ? estrella() : estrellaBorde(),
                    onTap: (){
                      //print("Estrella 3");
                      setState(() {
                        califTemp = 3;
                      });
                    }
                  ),
                  //ESTRELLA 4
                  GestureDetector(
                    child: califTemp>=4 ? estrella() : estrellaBorde(),
                    onTap: (){
                      //print("Estrella 4");
                      setState(() {
                        califTemp = 4;
                      });
                    }
                  ),
                  //ESTRELLA 5
                  GestureDetector(
                    child: califTemp==5 ? estrella() : estrellaBorde(),
                    onTap: (){
                      //print("Estrella 5");
                      setState(() {
                        califTemp = 5;
                      });
                    }
                  )
                ],
              ),
            ],
          );
        }
      ),
      actions: <Widget>[
        FlatButton(
          child: Text(
            "Eliminar de mi lista",
            style: TextStyle(
              color: Colors.red
            ),
          ), 
          onPressed: ()async{
            await _dbHelper.deleteContact(id);
            actualizarLista();
            Navigator.pop(context);
          }
        ),
        FlatButton(
          child: estado.contains("Calificado")?Text("Guardar"):Text("Calificar"), 
          onPressed: () async {
            //Si no está calificado se guarda por primera vez en firebase
            if(!estado.contains("Calificado")){
              //print("PRimera vez");
              final auth = FirebaseAuth.instance;
              CollectionReference users = FirebaseFirestore.instance.collection("serviciosCalificados");
              users.add({
                "usuario" : auth.currentUser.email,
                "calificacion": califTemp,
                "alberquero": listaOrdenada[index].alberquero,
                "servicio": listaOrdenada[index].servicio,
                "fecha" : DateTime.now()
              }).then((value) {
                print("Calificación agregada");
              }).catchError((error) => print("Error al agregar calificación: $error"));
            }else{
              //Si ya está calificado se modifica la calificación en firebase
              CollectionReference collectionReference = FirebaseFirestore.instance.collection("serviciosCalificados");
              QuerySnapshot querySnapshot = await collectionReference.get();

              //BUSCAR EL INDICE A EDITAR
              List docos = querySnapshot.docs;
              List datas = [];
              for (var data in docos) {
                datas.add(data.data());
              }
              // print(datas);
              // print("Modificación");
              // print(datas.where((element) => 
              //   element["alberquero"]==alberquero&&element["calificacion"]==calificacion&&element["servicio"]==servicio
              // ));
              int indice = datas.indexWhere((element){
                return element["alberquero"]==alberquero&&element["calificacion"]==calificacion&&element["servicio"]==servicio;
              });
              print(indice);
              // print(datas.indexWhere((element){
              //   return element["alberquero"]==alberquero&&element["calificacion"]==calificacion&&element["servicio"]==servicio;
              // }));

              //EDITAR EN FIREBASE USANDO EL INDICE
              querySnapshot.docs[indice].reference.update({"calificacion":califTemp});

            }
            calificarServicio(index, califTemp, _timeString);
            actualizarLista();
            Navigator.pop(context);
          }),
        FlatButton(child: Text("Cancelar"), onPressed: (){
          Navigator.pop(context);
        }),
      ]
    );
    showDialog(context: context, builder: (BuildContext context) {return dialog;});
  }

  calificarServicio(int index, int calif, String fecha)async{
    int id = listaOrdenada[index].id;
    String servicio = listaOrdenada[index].servicio;
    String alberquero = listaOrdenada[index].alberquero;
    String fecha = listaOrdenada[index].fecha;
    String estado = listaOrdenada[index].estado;
    int calificacion = listaOrdenada[index].calificacion;

    estado = "Calificado";
    calificacion = calif;

    await _dbHelper.updateContact(
      ServiciosModeloDB(
        id: id,
        servicio: servicio,
        alberquero: alberquero,
        fecha: fecha,
        estado: estado,
        calificacion: calificacion
      )
    );

  }

  actualizarLista() async{
    List<ServiciosModeloDB> x = await _dbHelper.fetchContacts();
    setState(() {
      listaServices = x;
      listaOrdenada = listaServices;
      listaOrdenada = listaServices.reversed.toList();
      //listaOrdenada.sort((a,b)=>int.parse(a.fecha).compareTo(int.parse(b.fecha)));
    });
  }

  Widget mostrarIcon(int index){
    String estado = listaOrdenada[index].estado;
    if(estado.contains("Calificado")){
      // return Icon(
      //   Icons.edit
      // );
      return Icon(
        Icons.star,
        color: Colors.orangeAccent,
        size: 30,
      );
    }
    return Text("");
  }


  Text mostrarEstado(int index){
    String estado = listaOrdenada[index].estado;
    int calificacion = listaOrdenada[index].calificacion;
    if(estado.contains("Pendiente")){
      return Text(
        "Pendiente",
        style: TextStyle(
          color: Colors.orange,
          fontWeight: FontWeight.bold
        ),
      );
    }
    if(estado.contains("Finalizado")){
      return Text(
        "Calificar",
        style: TextStyle(
          color: Colors.blue,
          fontWeight: FontWeight.bold
        ),
      );
    }
    if(estado.contains("Cancelado")){
      return Text(
        "Cancelado",
        style: TextStyle(
          color: Colors.red,
          fontWeight: FontWeight.bold
        ),
      );
    }
    if(estado.contains("Calificado")){
      return Text(
        calificacion.toString()+".0",
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 18
        ),
      );
    }
    return Text(estado);
  }

  Card mostrarCartaServicio(int index){
    String servicio = listaOrdenada[index].servicio;
    String alberquero = listaOrdenada[index].alberquero;
    String estado = listaOrdenada[index].estado;
    String fecha = listaOrdenada[index].fecha;
    // String servicio = listaServices[index].servicio;
    // String alberquero = listaServices[index].alberquero;
    // String estado = listaServices[index].estado;
    // String fecha = listaServices[index].fecha;

    return Card(
      child: ListTile(
        onTap: (){
          int fechHoy = int.parse(fechaActual);
          int fechCalif = int.parse(fecha);
          if(fechHoy<=fechCalif){
            showAlert(index);
          }else{
            mostrarEstrellas(index);
            print("No editable");
          }
          print("Fecha y hora límite: "+fecha);
          print("Fecha y hora actual: "+fechaActual);
        },
        title: Text(servicio),
        subtitle: Text(alberquero),
        trailing: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            mostrarEstado(index),
            mostrarIcon(index)
          ],
        )
      )
    );
  }

  mostrarLista()async{
    List<Map> x = await _dbHelper.fetchContactsMap();
    //print(x[0]);
    //print(x);
    listaServices = await _dbHelper.fetchContacts();
    listaOrdenada = listaServices;
    listaOrdenada = listaServices.reversed.toList();
    //listaOrdenada.sort((a,b)=>int.parse(a.fecha).compareTo(int.parse(b.fecha)));
    //print(listaServices.length.toString());
    //print(listaServices[0].fecha);
  }

  @override
  Widget build(BuildContext context) {
    mostrarLista();
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 0,
            ),
            Container(
              height: 40,
              //color: Colors.green,
              child: Stack(
                children: <Widget>[
                  Center(
                    child: Text(
                      "Mi lista de servicios", 
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Container(
                      padding: EdgeInsets.only(left: 30),
                      child: GestureDetector(
                        child: Material(
                          elevation: 20,
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(100),
                            topRight: Radius.circular(100),
                            bottomLeft: Radius.circular(100),
                            bottomRight: Radius.circular(100),
                          ),
                          child: CircleAvatar(
                            radius: 18,
                            backgroundColor: wasserColor,
                            child: Icon(
                              Icons.add,
                              size: 15,
                              color: Colors.white,
                            ),
                          ),
                        ), 
                        onTap: (){
                          print("Add");
                          Navigator.push(
                            context, 
                            MaterialPageRoute(builder: (_)=>ElegirServicio())
                          );
                        }
                      ),
                    ),
                  )
                ],
              ),
            ),
            Container(
              height: 20,
              //color: Colors.blue,
              child: Align(
                alignment: Alignment.topRight,
                child: Container(
                  padding: EdgeInsets.only(right: 5),
                  child: DropdownButton(
                    elevation: 0,
                    value: _ordenamientoSeleccionado,
                    items: _dropdownMenuItems, 
                    onChanged: (value){
                      onChangeDropdownItem(value);
                      //print(value.toString());
                      //print(_ordenamientoSeleccionado.ordenamiento);
                      String orden = _ordenamientoSeleccionado.ordenamiento;
                      //print(orden);
                      setState(() {
                        // print(listaServices[0].alberquero);
                        //List<ServiciosModeloDB> nueva = listaServices.reversed.toList();
                        // listaOrdenada = listaServices.reversed.toList();
                        // print(listaOrdenada[0].alberquero);
                        if(orden=="Reciente"){
                          print("Reciente");
                          listaOrdenada = listaServices.reversed.toList();
                          //listaOrdenada = listaServices;
                          //listaOrdenada.sort((a,b)=>int.parse(a.fecha).compareTo(int.parse(b.fecha)));
                          //print(listaOrdenada[0].fecha);
                        }
                        if(orden=="Antiguo"){
                          print("Antiguo");
                          listaOrdenada = listaServices;
                          //listaOrdenada.sort((b,a)=>a.fecha.compareTo(b.fecha));
                          //listaOrdenada.sort((b,a)=>int.parse(a.fecha).compareTo(int.parse(b.fecha)));
                        }
                        if(orden=="Alberquero"){
                          print("Alberquero");
                          listaOrdenada = listaServices;
                          listaOrdenada.sort((a,b)=>a.alberquero.compareTo(b.alberquero));
                        }
                        if(orden=="Calificación"){
                          print("Calificación");
                          listaOrdenada = listaServices;
                          listaOrdenada.sort((b,a)=>a.calificacion.compareTo(b.calificacion));
                        }
                        if(orden=="Servicio"){
                          print("Servicio");
                          listaOrdenada = listaServices;
                          listaOrdenada.sort((a,b)=>a.servicio.compareTo(b.servicio));
                        }
                      });
                      
                      //print(_ordenamientoSeleccionado.ordenamiento);
                    },
                    //style: ,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            ListView.builder(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: listaOrdenada.length,
              itemBuilder: (context, index){
                //return Text("$index");
                String fecha = listaOrdenada[index].fecha.substring(0,8);
                String fechaLegible = "";

                fechaLegible += fecha.substring(6);
                fechaLegible += "/";
                fechaLegible += fecha.substring(4,6);
                fechaLegible += "/";
                fechaLegible += fecha.substring(0,4);
                //print(fecha);
                if(index==0){
                  return Column(
                    children: <Widget>[
                      Text(fechaLegible),
                      mostrarCartaServicio(index)
                    ],
                  );
                }else{
                  if(fecha==listaOrdenada[index-1].fecha.substring(0,8)){
                    return mostrarCartaServicio(index);
                  }else{
                    return Column(
                      children: <Widget>[
                        Text(fechaLegible),
                        mostrarCartaServicio(index)
                      ],
                    );
                  }
                }
              }
            )
          ],
        ),
      ),
    );
  }
}

class AgregarServicio extends StatefulWidget {
  @override
  _AgregarServicioState createState() => _AgregarServicioState();
}

class _AgregarServicioState extends State<AgregarServicio> {

  DatabaseHelper _dbHelper;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      _dbHelper = DatabaseHelper.instance;
    });
  }

  TextEditingController conServicio = TextEditingController();
  TextEditingController conAlberquero = TextEditingController();
  TextEditingController conFecha = TextEditingController();
  TextEditingController conEstado = TextEditingController();
  TextEditingController conCalificacion = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Agregar servicio"),),
      body: Column(
        children: <Widget>[
          TextField(
            decoration: InputDecoration(hintText: "Servicio"),
            controller: conServicio,
          ),
          TextField(
            decoration: InputDecoration(hintText: "Alberquero"),
            controller: conAlberquero,
          ),
          TextField(
            decoration: InputDecoration(hintText: "Fecha"),
            controller: conFecha,
          ),
          TextField(
            decoration: InputDecoration(hintText: "Estado"),
            controller: conEstado,
          ),
          TextField(
            decoration: InputDecoration(hintText: "Calificacion"),
            controller: conCalificacion,
          ),
          RaisedButton(
            onPressed: ()async{
              await _dbHelper.insertContact(ServiciosModeloDB(
                //id: 0,
                servicio: conServicio.text,
                alberquero: conAlberquero.text,
                fecha: conFecha.text,
                estado: conEstado.text,
                calificacion: int.parse(conCalificacion.text)
              ));
              Navigator.pop(context);
            },
            child: Text("Agregar a la lista"),
          )
        ],
      ),
    );
  }
}


  class Ordenamiento2 {
  int id;
  String ordenamiento;

  Ordenamiento2({this.id, this.ordenamiento});

  static List<Ordenamiento2> getordenamientos(){
    return <Ordenamiento2>[
      Ordenamiento2(id: 0, ordenamiento: "Calificación"),
      Ordenamiento2(id: 1, ordenamiento: "Alberquero"),
      Ordenamiento2(id: 2, ordenamiento: "Total servicios")
    ];
  }
}

class Lista2 extends StatefulWidget {
  @override
  _Lista2State createState() => _Lista2State();
}

class _Lista2State extends State<Lista2> {

  List<Ordenamiento2> _ordenamientos = Ordenamiento2.getordenamientos();
  List<DropdownMenuItem<Ordenamiento2>> _dropdownMenuItems;
  Ordenamiento2 _ordenamientoSeleccionado;

  DatabaseHelper _dbHelper;

    @override
  void initState() {
    // TODO: implement initState
    
    super.initState();
    _dropdownMenuItems = buildDropDownMenuItems(_ordenamientos);
    _ordenamientoSeleccionado = _dropdownMenuItems[0].value;
    obtenerAlberqueros();
    //print("inicio");

    setState(() {
      _dbHelper = DatabaseHelper.instance;
    });
    actualizarLista();
  }

  actualizarLista() async{
    List<ServiciosModeloDB> x = await _dbHelper.fetchContacts();
    setState(() {
      listaServices = x;
    });
  }

  List<DropdownMenuItem<Ordenamiento2>> buildDropDownMenuItems(List ordenamientos){
    List<DropdownMenuItem<Ordenamiento2>> items = List();
    for (Ordenamiento2 ordenamiento in ordenamientos) {
      items.add(
        DropdownMenuItem(
          value: ordenamiento,
          child: Text(ordenamiento.ordenamiento)
        )
      );
    }
    return items;
  }

  onChangeDropdownItem(Ordenamiento2 seleccionado){
    setState(() {
      _ordenamientoSeleccionado = seleccionado;
    });
  }

  List<ServiciosModeloDB> listaPrincipal = listaServices;
  List<AlberquerosContratados> contratados = [];

  String alberquero = "";
  int totalServicios = 0;
  int serviciosCalificados = 0;
  double promedio = 0;

  obtenerAlberqueros(){
    //print(listaPrincipal);
    //print(contratados);
    while(listaPrincipal.length>0){
      //Toma el nombre del primer alberquero
      alberquero = listaPrincipal[0].alberquero;
      //Crea una lista temporal donde se repite el nombre
      List<ServiciosModeloDB> listTemp = listaPrincipal.where((element) => element.alberquero==alberquero).toList();
      //Elimina esos datos de la lista principal
      listaPrincipal.removeWhere((element) => element.alberquero == alberquero);
      //Obtiene la cantidad total de servicios para asignar a la variable totalServicios
      totalServicios = listTemp.length;
      //Crea una segunda lista temporal donde se guardan sólo los servicios calificados
      List<ServiciosModeloDB> listTemp2 = listTemp.where((e) => e.estado=="Calificado").toList();
      //Obtiene la cantidad de servicios calificados y los asigna a la variable serviciosCalificados
      serviciosCalificados = listTemp2.length;
      //Si la lista de servicios calificados esta vacia el promedio es 0
      //Si la lista de servicios calificados (temp2) tiene uno o más valores se realiza el cálculo
      if(serviciosCalificados>0){
        //Variable para almacenar la sumatoria
        int sumatoria = 0;
        //Obtener la sumatoria de calificaciones
        for (var item in listTemp2) {
          sumatoria += item.calificacion;
        }
        //Calcular el promedio dividiendo la sumatoria entre la cantidad de servicios calificados
        promedio = sumatoria / serviciosCalificados;
      }
      //Agregar estos datos (alberquero, total servcios, servicios calificados y promedio) a la lista alberqueros contratados
      contratados.add(
        AlberquerosContratados(
          alberquero: alberquero, 
          totalServicios: totalServicios, 
          serviciosCalificados: serviciosCalificados,
          promedio: promedio
        )
      );
    }
    // for (var item in contratados) {
    //   print(item.alberquero);
    //   print(item.totalServicios);
    //   print(item.serviciosCalificados);
    //   print(item.promedio);
    // }
    contratados.sort((b,a)=>a.promedio.compareTo(b.promedio));
  }


  void showAlert2(int index){
    String alberquero = contratados[index].alberquero;
    int total = contratados[index].totalServicios;
    int calificados = contratados[index].serviciosCalificados;
    double promedio = contratados[index].promedio;
    //print(listaServices);
    AlertDialog dialog = AlertDialog(
      title: Center(
        child: Text("Promedio de alberquero"),
      ),
      content: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        //crossAxisAlignment: CrossAxisAlignment.stretch,
        //mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                alberquero,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(
                "Servicios calificados: $calificados\nServicios recibidos: $total",
                style: TextStyle(
                  fontSize: 14,
                  color: Colors.grey[600]
                ),
              ),
            ],
          ),
          Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text("$promedio", style: TextStyle(
                fontWeight: FontWeight.bold
              ),),
              Icon(Icons.star, color: Colors.orange),
            ],
          )
        ],
      ),
      actions: <Widget>[
        FlatButton(child: Text("Salir"), onPressed: (){
          Navigator.pop(context);
        }),
      ]
    );
    showDialog(context: context, builder: (BuildContext context) {return dialog;});
  }

  Card mostrarCarta(int index){
    String alberqueroC = contratados[index].alberquero;
    int totalC = contratados[index].totalServicios;
    //int calificados = contratados[index].serviciosCalificados;
    double promedioC = contratados[index].promedio;

    return Card(
      child: ListTile(
        onTap: (){
          showAlert2(index);
        },
        title: Text(alberqueroC),
        subtitle: Text("Total de servicios: $totalC"),
        trailing: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text("${promedioC.toString().substring(0,3)}", style: TextStyle(
              fontWeight: FontWeight.bold
            ),),
            Icon(Icons.star, color: Colors.orange)
          ],
        )
      )
    );
  }

  @override
  Widget build(BuildContext context) {
    //Este método lo llamo en el initstate, si no no permite aplicar el ordenamiento de la lista
    //obtenerAlberqueros();
    // if(contratados.length==0){
    //   obtenerAlberqueros();
    // }
    //print("build");
    return Scaffold(
      //backgroundColor: Colors.red,
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.only(top: 10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                child: Center(
                  child: Text(
                    "Historial de alberqueros contratados", 
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20
                    ),
                  ),
                ),
              ),
              SizedBox(height: 3),
              contratados.length==0?
              Container(
                height: MediaQuery.of(context).size.height/2,
                child: Center(child: Text("Sin datos para mostrar"))
              )
              :
              Container(
                height: 15,
                //color: Colors.blue,
                child: Align(
                  alignment: Alignment.topRight,
                  child: Container(
                    padding: EdgeInsets.only(right: 5),
                    child: DropdownButton(
                      elevation: 0,
                      value: _ordenamientoSeleccionado,
                      items: _dropdownMenuItems, 
                      onChanged: (value){
                        onChangeDropdownItem(value);
                        //print(value.toString());
                        //print(_ordenamientoSeleccionado.ordenamiento);
                        String orden = _ordenamientoSeleccionado.ordenamiento;
                        //print(orden);
                        setState(() {
                          // print(listaServices[0].alberquero);
                          //List<ServiciosModeloDB> nueva = listaServices.reversed.toList();
                          // listaOrdenada = listaServices.reversed.toList();
                          // print(listaOrdenada[0].alberquero);
                          if(orden=="Alberquero"){
                            print("Alberquero");
                            contratados.sort((a,b)=>a.alberquero.compareTo(b.alberquero));
                            // contratados = contratados.reversed.toList();
                            // for (var item in contratados) {
                            //   print(item.alberquero);
                            // }
                            //contratados = [];
                            //listaOrdenada = listaServices;
                            //listaOrdenada.sort((a,b)=>a.alberquero.compareTo(b.alberquero));
                          }
                          if(orden=="Calificación"){
                            contratados.sort((b,a)=>a.promedio.compareTo(b.promedio));
                            print("Calificación");
                            //listaOrdenada = listaServices;
                            //listaOrdenada.sort((b,a)=>a.calificacion.compareTo(b.calificacion));
                          }
                          if(orden=="Total servicios"){
                            print("Servicio");
                            contratados.sort((b,a)=>a.totalServicios.compareTo(b.totalServicios));
                            //listaOrdenada = listaServices;
                            //listaOrdenada.sort((a,b)=>a.servicio.compareTo(b.servicio));
                          }
                        });
                        
                        //print(_ordenamientoSeleccionado.ordenamiento);
                      },
                      //style: ,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 5),
              ListView.builder(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: contratados.length,
                itemBuilder: (context, index){
                  return mostrarCarta(index);
                }
              )
            ],
          ),
        ),
      ),
      // body: Container(
      //     padding: EdgeInsets.only(top: 10),
      //     child: Column(
      //       mainAxisAlignment: MainAxisAlignment.start,
      //       children: <Widget>[
      //         Text(
      //           "Historial de alberqueros contratados", 
      //           style: TextStyle(
      //             fontWeight: FontWeight.bold,
      //             fontSize: 20
      //           ),
      //         ),
      //         Card(
      //           child: ListTile(
      //             onTap: showAlert2,
      //             title: Text("Manuel Pérez"),
      //             subtitle: Text("Total de servicios: 2"),
      //             trailing: Column(
      //               mainAxisAlignment: MainAxisAlignment.center,
      //               children: <Widget>[
      //                 Text("5.0", style: TextStyle(
      //                   fontWeight: FontWeight.bold
      //                 ),),
      //                 Icon(Icons.star, color: Colors.orange)
      //               ],
      //             )
      //           )
      //         ),
      //         Card(
      //           child: ListTile(
      //             title: Text("Juan López"),
      //             subtitle: Text("Total de servicios: 5"),
      //             trailing: Column(
      //               mainAxisAlignment: MainAxisAlignment.center,
      //               children: <Widget>[
      //                 Text("4.5", style: TextStyle(
      //                   fontWeight: FontWeight.bold
      //                 ),),
      //                 Icon(Icons.star, color: Colors.orange)
      //               ],
      //             ),
      //             onTap: obtenerAlberqueros,
      //           )
      //         ),
      //         //Text("15/9/20"),
      //         Card(
      //           child: ListTile(
      //             title: Text("Miguel Ramirez"),
      //             subtitle: Text("Total de servicios: 7"),
      //             trailing: Column(
      //               mainAxisAlignment: MainAxisAlignment.center,
      //               children: <Widget>[
      //                 Text("4.2", style: TextStyle(
      //                   fontWeight: FontWeight.bold
      //                 ),),
      //                 Icon(Icons.star, color: Colors.orange)
      //               ],
      //             )
      //           )
      //         ),
      //         //Text("22/9/20"),
      //         Card(
      //           child: ListTile(
      //             title: Text("Raul Flores"),
      //             subtitle: Text("Total de servicios: 1"),
      //             trailing: Column(
      //               mainAxisAlignment: MainAxisAlignment.center,
      //               children: <Widget>[
      //                 Text("4.0", style: TextStyle(
      //                   fontWeight: FontWeight.bold
      //                 ),),
      //                 Icon(Icons.star, color: Colors.orange)
      //               ],
      //             )
      //           )
      //         ),
      //       ]
      //     )
      // )
    );
  }
}


class ServiciosModeloDB {
  static const tblServicios = 'servicios';
  static const colId = 'id';
  static const colServicio = 'servicio';
  static const colAlberquero = 'alberquero';
  static const colFecha = 'fecha';
  static const colEstado = 'estado';
  static const colCalificacion = 'calificacion';

  int id;
  String servicio;
  String alberquero;
  String fecha;
  String estado;
  int calificacion;

  ServiciosModeloDB({this.id,this.servicio, this.alberquero, this.fecha, this.estado, this.calificacion});

  //Constructor nombrado, convierte MAP a OBJETO
  ServiciosModeloDB.fromMap(Map<String, dynamic> map){
    id = map[colId];
    servicio = map[colServicio];
    alberquero = map[colAlberquero];
    fecha = map[colFecha];
    estado = map[colEstado];
    calificacion = map[colCalificacion];
  }

  //CONVIERTE OBJETO A MAP
  Map<String, dynamic> toMap(){
    var map = <String, dynamic>{
      colServicio: servicio,
      colAlberquero: alberquero,
      colFecha: fecha,
      colEstado: estado,
      colCalificacion: calificacion
    };
    if(id!=null) map[colId] = id;
    return map;
  }

}

class Lista3 extends StatelessWidget {

  int tam = 0;

  //PROCEDIMIENTO PARA PROMEDIO DE 10 MEJORES ALBERQUEROS:
  //1. Obtener la lista completa de servicios de firebase
  //2. Obtener listas de cada alberquero con la cantidad de servicios realizados y calificaciones
  //3. Sacar el promedio de calificaciones de esas listas
  //4. Crear una lista de 10 elementos con las calificaciones más altas

  //CAMPOS: alberquero, calificacion, servicio

  //LISTA DE MODELOS
  List<ServicioSolicitadoModel> listaPrincipal = [];

  //CREAR LISTA DE MODELOS EN BASE A LA LISTA DE DOCUMENTOS
  crearListaSolicitadosModel(){
    List<ServicioSolicitadoModel> listaServices = [];
    for (var item in least) {
      ServicioSolicitadoModel modeloTemp = ServicioSolicitadoModel(
        alberquero: item['alberquero'],
        servicio: item['servicio'],
        calificacion: item['calificacion']
      );
      listaServices.add(modeloTemp);
    }
    listaPrincipal = listaServices;
  }

  //CREAR LISTA DE MEJORES ALBERQUEROS VACÍA
  List<MejoresAlberqueros> mejores = [];
  //Luego se va a aplicar .sort() para ordenar.
  //Y de último .take(10) para tomar 10 valores de esa lista.
  List<MejoresAlberqueros> listaMejores = [];

  obtenerMejores(){
    String alberquero = "";
    int totalServicios = 0;
    double promedio = 0;
    //print(listaPrincipal.length);
    while(listaPrincipal.length>0){
      //Toma el nombre del alberquero de la posición 0
      alberquero = listaPrincipal[0].alberquero;
      //Crea una lista documentos de donde se repite ese nombre
      List<ServicioSolicitadoModel> listTemp = listaPrincipal.where((element) => element.alberquero == alberquero).toList();
      //Elimina esos documentos de la lista principal
      listaPrincipal.removeWhere((element) => element.alberquero == alberquero);
      //Obtiene la cantidad de servicios y la asigna a la variable totalServicios
      totalServicios = listTemp.length;
      //print(alberquero);
      //Obtener la sumatoria de calificaciones
      int sumatoria = 0;
      for (var item in listTemp) {
        sumatoria += item.calificacion;
      }
      //Calcular el promedio dividiendo la sumatoria entre el total
      promedio = sumatoria / totalServicios;

      //Agregar a la lista de mejores alberqueros
      mejores.add(
        MejoresAlberqueros(alberquero: alberquero, totalServicios: totalServicios, promedio: promedio)
      );
    }

    //Ordena la lista por promedio de menor a mayor
    mejores.sort((a,b)=>a.promedio.compareTo(b.promedio));
    
    //Imprime el tamaño de la lista
    //print(mejores.length.toString());
    //Imprime los promedios de los elementos de la lista
    //mejores.forEach((element)=>print(element.promedio));

    //Crea una nueva lista ordenando de mayor a menor los promedios y tomando sólo los primeros 10 elementos
    //List<MejoresAlberqueros> list = mejores.reversed.take(10).toList();
    listaMejores = mejores.reversed.take(10).toList();
    listaPrincipal = [];
    mejores = [];
  }

  List least = [];

  Card cartaMejor(int index){
    return Card(
      margin: EdgeInsets.all(2),
      child: ListTile(
        leading: CircleAvatar(
          backgroundColor: Color.fromRGBO(17, 167, 174, 1),
          child: Text(
            "${index+1}",
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
              color: Colors.white
            ),
          ),
        ),
        title: Text(listaMejores[index].alberquero),
        subtitle: Text("Total servicios: ${listaMejores[index].totalServicios}"),
        trailing: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(listaMejores[index].promedio.toString().substring(0,3), style: TextStyle(
              fontWeight: FontWeight.bold
            ),),
            Icon(Icons.star, color: Colors.orange)
          ],
        )
      )
    );
  }
  
  @override
  Widget build(BuildContext context) {
    listaPrincipal = [];
    mejores = [];
    listaMejores = [];
    least = [];
    return Scaffold(
      body: StreamBuilder(
        stream: FirebaseFirestore.instance.collection('serviciosCalificados').snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot){
          if (!snapshot.hasData) return Center(child: CircularProgressIndicator());
          tam = snapshot.data.docs.length;
          for (var i = 0; i < tam; i++) {
            least.add(snapshot.data.docs[i].data());
          }
          //print(least);
          crearListaSolicitadosModel();
          obtenerMejores();
          return Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(5),
                child: Text(
                  "10 Mejor Valorados",
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold
                  ),
                ),
              ),
              Expanded(
                child: ListView.builder(
                  itemCount: listaMejores.length,
                  itemBuilder: (context, int index){
                    return Card(
                      child: Column(
                        children: <Widget>[ //Card con datos de los mejores
                          cartaMejor(index)
                        ],
                      ),
                    );
                  },
                ),
              ),
            ],
          );
        }
      ),
    );
  }
}

class AlberqueroCardItem extends StatelessWidget {
  final MejoresAlberqueros mejor;
  AlberqueroCardItem({this.mejor});
  
  @override
  Widget build(BuildContext context) {
    return /*Card(
      margin: EdgeInsets.all(2),
      child: */ListTile(
        title: Text(mejor.alberquero),
        subtitle: Text("Total servicios: ${mejor.totalServicios.toString()}"),
        trailing: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(mejor.promedio.toString(), style: TextStyle(
              fontWeight: FontWeight.bold
            ),),
            Icon(Icons.star, color: Colors.orange)
          ],
        )
      //)
    );
  }
}

class ServicioSolicitadoModel{
  String alberquero;
  String servicio;
  int calificacion;

  ServicioSolicitadoModel({this.alberquero, this.servicio, this.calificacion});

  ServicioSolicitadoModel.fromMap(Map<String, dynamic> map){
    alberquero = map['alberquero'];
    servicio = map['servicio'];
    calificacion = map['calificacion'];
  }

  Map<String, dynamic> toMap(){
    var map = <String, dynamic>{
      'alberquero': alberquero,
      'servicio': servicio,
      'calificacion': calificacion,
    };
    return map;
  }
}

class MejoresAlberqueros {
  String alberquero = "";
  int totalServicios = 0;
  double promedio = 0.0;

  MejoresAlberqueros({this.alberquero, this.totalServicios, this.promedio});
}

class AlberquerosContratados {
  String alberquero = "";
  int serviciosCalificados = 0;
  int totalServicios = 0;
  double promedio = 0.0;

  AlberquerosContratados({this.alberquero, this.serviciosCalificados, this.totalServicios, this.promedio});
}