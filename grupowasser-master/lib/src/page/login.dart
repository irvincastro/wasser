import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:grupo_wasser/main.dart';
import 'package:grupo_wasser/src/datos/firebaseAut.dart';
import 'package:grupo_wasser/src/page/registroUser.dart';
import 'package:grupo_wasser/src/style/style.dart';
import 'package:toast/toast.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

final FirebaseAuth auth = FirebaseAuth.instance;

class _LoginScreenState extends State<LoginPage> {
  bool mostrar = true;
  UserRepository userRepository;
  TextEditingController correo = TextEditingController();
  TextEditingController pass = TextEditingController();
  final _formLogin = GlobalKey<FormState>();

  User user;
  void initState() {
    super.initState();

    userRepository = UserRepository();
  }

  Widget _buildEmailTF() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Correo',
          style: kLabelStyle,
        ),
        SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: kBoxDecorationStyle,
          height: 60.0,
          child: TextFormField(
            validator: (value) {
              if (value.isEmpty) {
                return 'El campo es obligatorio';
              }
              if (validarEmail(value)) {
                Toast.show('correo invalido', context,
                    duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
                return 'correo invalido';
              }
            },
            controller: correo,
            keyboardType: TextInputType.emailAddress,
            style: TextStyle(
              color: Colors.white,
              fontFamily: 'OpenSans',
            ),
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              prefixIcon: Icon(
                Icons.email,
                color: Colors.white,
              ),
              hintText: 'Correo',
              hintStyle: kHintTextStyle,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildPasswordTF() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Contraseña',
          style: kLabelStyle,
        ),
        SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: kBoxDecorationStyle,
          height: 60.0,
          child: TextFormField(
            validator: (value) {
              if (value.isEmpty) {
                return 'El campo es obligatorio';
              }
            },
            controller: pass,
            obscureText: mostrar,
            style: TextStyle(
              color: Colors.white,
              fontFamily: 'OpenSans',
            ),
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              prefixIcon: Icon(
                Icons.lock,
                color: Colors.white,
              ),
              hintText: 'Contraseña',
              hintStyle: kHintTextStyle,
              suffixIcon: IconButton(
                icon: Icon(
                  mostrar ? Icons.visibility_off : Icons.visibility,
                  color: Colors.white,
                ),
                onPressed: () {
                  setState(() {
                    mostrar = !mostrar;
                  });
                },
              ),
            ),
          ),
        ),
      ],
    );
  }

  void inciarSesion() async {
    if (_formLogin.currentState.validate()) {
      if (validarEmail(correo.text) == true) {
        BuildContext circular = context;
                //aqui meti el spinner de espera de carga
                showDialog(
                    context: circular,
                    builder: (BuildContext context) {
                      return SpinKitSquareCircle(
                        color: Color.fromRGBO(255, 255, 255, 1),
                        size: 80,
                      );
                    });
        try {
          user = (await auth.signInWithEmailAndPassword(
            email: correo.text,
            password: pass.text,
          ))
              .user;
          print("$user===========>");
          if (user != null) {
            setState(() {
              prefs.correo = user.email;
            });
            getUser().then((value) {
              print(value.data()["nombre"]);
              setState(() {
                prefs.nombreUsuario =
                    "${value.data()["nombre"]} ${value.data()["PrimerApellido"]}";
                prefs.tipo = int.parse(value.data()["tipo"].toString());
              });
              if (value.data()["tipo"] == 0) {
                Navigator.pop(circular);
                Navigator.pushNamedAndRemoveUntil(context, "/inicio", (route) => false);
               
              } else {
                Navigator.pop(circular);
                prefs.activo = value.data()["Availability"];
                Navigator.pushNamedAndRemoveUntil(context, "/worker", (route) => false);
                
              }
            });

            //
          }
        } on FirebaseAuthException catch (e) {
          Navigator.pop(circular);
          Toast.show('correo o contraseña incorrecta', context,
              duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
          print(e);
        }
      }else{
         Toast.show('correo invalido', context,
                    duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
      }
    }
  }

  Widget botonRegistro() {
    return Container(
      padding: EdgeInsets.fromLTRB(40, 2, 40, 2),
      child: ButtonTheme(
        minWidth: double.infinity,
        child: RaisedButton(
          elevation: 0,
          color: Colors.transparent,
          textColor: Colors.white,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Registro()),
            );
          },
          child: Text('Registrarse'),
        ),
      ),
    );
  }

  Widget _buildLoginBtn() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 25.0),
      width: double.infinity,
      child: RaisedButton(
        elevation: 5.0,
        onPressed: () async {
          // User user = (await auth.signInWithCredential()).user;
          inciarSesion();
          //Navigator.pushNamed(context, '/inicio');
        },
        padding: EdgeInsets.all(15.0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30.0),
        ),
        color: Colors.white,
        child: Text(
          'INICIAR SESIÓN',
          style: TextStyle(
            color: Color.fromRGBO(22, 189, 171, 1),
            letterSpacing: 1.5,
            fontSize: 18.0,
            fontWeight: FontWeight.bold,
            fontFamily: 'OpenSans',
          ),
        ),
      ),
    );
  }

  Widget _buildSocialBtn(Function onTap, AssetImage logo) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: 60.0,
        width: 60.0,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.black26,
              offset: Offset(0, 2),
              blurRadius: 6.0,
            ),
          ],
          image: DecorationImage(
            image: logo,
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: Center(
          child: GestureDetector(
            onTap: () => FocusScope.of(context).unfocus(),
            child: Stack(
              children: <Widget>[
                Container(
                  height: double.infinity,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [
                        Color.fromRGBO(17, 167, 174, 1),
                        Color.fromRGBO(22, 189, 171, 1),
                        Color.fromRGBO(22, 189, 148, 1),
                        Color.fromRGBO(22, 189, 143, 1),
                      ],
                      stops: [0.1, 0.4, 0.7, 0.9],
                    ),
                  ),
                ),
                Center(
                  child: Container(
                    //height: double.infinity,
                    child: SingleChildScrollView(
                      physics: AlwaysScrollableScrollPhysics(),
                      padding: EdgeInsets.symmetric(
                        horizontal: 40.0,
                        vertical: 120.0,
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Wrap(
                            children: [
                              Text(
                                'Grupo ',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontFamily: 'OpenSans',
                                  fontSize: 30.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Text(
                                'Wasser',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontFamily: 'OpenSans',
                                  fontSize: 30.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 30.0),
                          _buildEmailTF(),
                          SizedBox(
                            height: 30.0,
                          ),
                          Form(
                            key: _formLogin,
                            child: Column(
                              children: [
                                _buildPasswordTF(),
                                _buildLoginBtn(),
                              ],
                            ),
                          ),
                          botonRegistro(),
                        ],
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  bool validarEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    return (!regex.hasMatch(value)) ? false : true;
  }
}
