/*
*     Vista para el inicio de sesion
*/


// ------------------------------|>| Importaciones |<|------------------------------
import 'package:flutter/material.dart';
import 'package:grupo_wasser/src/page/worker.dart';
import 'geoMain.dart';

// ------------------------------|>| Constructor |<|------------------------------
class DefaultLogin extends StatefulWidget {
  @override
  _DefaultLoginState createState() => _DefaultLoginState();
}

class _DefaultLoginState extends State<DefaultLogin> {
// ------------------------------|>| Variables |<|------------------------------
  // Llave para la validacion del formulario
  final _formKey = GlobalKey<FormState>();

  // Booleano para el despliegue de la pantalla de carga
  bool loading = false;

  // Parametros que van a ser usados para el formulario
  String email = '';
  String password = '';
  String error = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.brown[100],
      appBar: AppBar(
        backgroundColor: Colors.brown[400],
        elevation: 0.0,
        title: Text('Inicia sesión en wasser'),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 15.0),
        child: Form(
          key: _formKey,
          child: Column(
            children: <Widget>[
              // Campo para correo electronico
              SizedBox(
                height: 20.0,
              ),
              TextFormField(
                decoration: InputDecoration(
                    fillColor: Colors.white,
                    filled: true,
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.white, width: 2.0),
                    ),
                    focusedBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Colors.pink, width: 2.0))),
                validator: (val) => val.isEmpty ? 'Introduce e-mail' : null,
                onChanged: (val) {
                  setState(() => email = val);
                },
              ),
              // Campo para contraseña
              SizedBox(
                height: 20.0,
              ),
              TextFormField(
                decoration: InputDecoration(
                    fillColor: Colors.white,
                    filled: true,
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.white, width: 2.0),
                    ),
                    focusedBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Colors.pink, width: 2.0))),
                validator: (val) =>
                    val.length < 6 ? 'La contraseña debe ser mayor a 6 caracteres' : null,
                obscureText: true,
                onChanged: (val) {
                  setState(() => password = val);
                },
              ),
              // Boton de inicio de sesion
              SizedBox(
                height: 20.0,
              ),
              RaisedButton(
                  color: Colors.pink[400],
                  child: Text(
                    'Iniciar sesión',
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () async {
                    if (_formKey.currentState.validate()) {
                      setState(() => loading = true);
                      if (email == "1") {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  GeoMain()), // GeoMain Es para el cliente
                        );
                      } else if (email == "2") {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => WorkerView()),
                        );
                      }
                      // Comprobacion de formato de email valido
                      /*if (result == null) {
                        setState(() {
                          error = 'Email or Password not valid';
                          loading = true;
                        });
                      }*/
                    }
                  }),
              // Mensaje de error
              SizedBox(height: 12.0),
              Text(
                error,
                style: TextStyle(color: Colors.red, fontSize: 14.0),
              )
            ],
          ),
        ),
      ),
    );
  }
}
