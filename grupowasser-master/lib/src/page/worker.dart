/*
*     Vista dedicada a la funcionalidad del alberquero
*     - Habilitar/Deshabilitar disponibilidad
*     - Actualizar su ubicacion geografica en firebase
*/

// ------------------------------|>| Importaciones |<|------------------------------
import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:grupo_wasser/main.dart';
import 'package:grupo_wasser/src/widget/menu.dart';

import '../../main.dart';

// ------------------------------|>| Constructor |<|------------------------------
class WorkerView extends StatefulWidget {
  @override
  _WorkerViewState createState() => _WorkerViewState();
}

class _WorkerViewState extends State<WorkerView> {
// ------------------------------|>| Variables |<|------------------------------
  bool _timerON = false;
  Timer _timer;
  int _start = 0;
  int refreshTime = 15;
  bool _availability = false;
  CollectionReference alberquero =
      FirebaseFirestore.instance.collection("usaurios");

  @override
  void initState() {
    super.initState();
    getAvailability();
  }

  @override
  Widget build(BuildContext context) {
    if (_availability) {
      if (!_timerON) startTimer();
    } else {
      getAvailability();
    }
    if (_start <= 0) {
      _start = refreshTime;
      updateLocation();
    }

    return Scaffold(
        appBar: AppBar(
          title: Text("Alberquero"),
        ),
        drawer: MenuPage(),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                width: double.infinity,
                height: 50,
                child: Text(
                  _availability ? "Disponible" : "No disponible",
                  style: TextStyle(fontSize: 28, fontWeight: FontWeight.bold),
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(
                height: 60,
              ),
              SizedBox(
                width: 200,
                height: 80,
                child: RaisedButton(
                    color: _availability ? Colors.green : Colors.red,
                    child: Text(
                      _availability
                          ? "Desactivar disponibilidad"
                          : "Activar disponibilidad",
                      style: TextStyle(color: Colors.white, fontSize: 25),
                      textAlign: TextAlign.center,
                    ),
                    onPressed: () {
                      setState(() {
                        _availability = !_availability;
                        updateAvailability();
                        prefs.activo = _availability;

                        if (_availability) {
                          _start = refreshTime;
                          _timerON = false;
                        } else {
                          _timerON = true;
                          _start = refreshTime;
                          _timer.cancel();
                        }
                      });
                    }),
              ),
            ],
          ),
        ));
  }

  Future<bool> getAvailability() async {
    await alberquero
        .doc(prefs.correo)
        .get()
        .then((value) => _availability = value.data()['Availability']);
    return false;
  }

  Future<void> updateAvailability() async {
    await alberquero.doc(prefs.correo).update({
      'Availability': _availability ?? false,
    });
  }

  void startTimer() {
    _timerON = true;
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) => setState(
        () {
          if (_start < 1) {
            timer.cancel();
            _timerON = false;
          } else {
            _start = _start - 1;
          }
        },
      ),
    );
  }

  void updateLocation() async {
    Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high)
        .then((currloc) {
      setState(() {
        alberquero.doc(prefs.correo).update({
          'Location': GeoPoint(currloc.latitude, currloc.longitude),
        });
      });
    });
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }
}
