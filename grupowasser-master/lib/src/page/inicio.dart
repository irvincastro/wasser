import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:grupo_wasser/src/datos/datosTable.dart';
import 'package:grupo_wasser/src/datos/sqlite.dart';
import 'package:grupo_wasser/src/model/Piscina.dart';
import 'package:grupo_wasser/src/model/quimicos.dart';
import 'package:grupo_wasser/src/style/style.dart';
import 'package:grupo_wasser/src/widget/menu.dart';
import 'package:responsive_table/ResponsiveDatatable.dart';
import 'package:search_choices/search_choices.dart';
import 'package:sqflite/sqflite.dart';

import '../../main.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Piscina currentPiscina;
  List texto = List();
  bool recalcular = false;
  Future<List> piscinasAll;
  bool sinPiscinas = false;
  List<Map<String, dynamic>> temps = List<Map<String, dynamic>>();
  bool status = false;
  @override
  void initState() {
    super.initState();
    abrirBaseDatos();
  }

  abrirBaseDatos() async {
    piscinasAll = TaskDatabase.db.getallPiscinaUserCorreo(prefs.correo);
    if (piscinasAll == null) {
      setState(() {
        sinPiscinas = true;
      });
    }
  }

  calcularDatosQuimicos() {
    setState(() {
      recalcular = true;
    });
    for (var item in quimico) {
      if (item.id == 1) {
        item.cantidad = double.parse((currentPiscina.metroCubico / 50).toStringAsFixed(3));
      }
      if (item.id == 2) {
        item.cantidad = double.parse(
            (currentPiscina.metroCubico * 0.003).toStringAsFixed(3));
      }
      if (item.id == 3) {
        item.cantidad = double.parse(
            (currentPiscina.metroCubico * 0.005).toStringAsFixed(3));
      }
      if (item.id == 4) {
        item.cantidad = double.parse(
            (currentPiscina.metroCubico * 0.013).toStringAsFixed(3));
      }
      if (item.id == 5) {
        item.cantidad = double.parse(
            (currentPiscina.metroCubico * 0.01).toStringAsFixed(3));
      }
      if (item.id == 6) {
       item.cantidad = double.parse(
            (currentPiscina.metroCubico * 0.01).toStringAsFixed(3));}
      if (item.id == 7) {
        item.cantidad = double.parse(
            (currentPiscina.metroCubico * 0.01).toStringAsFixed(3));
      }
      if (item.id == 8) {
        item.cantidad = double.parse(
            (currentPiscina.metroCubico * 0.002).toStringAsFixed(3));
      }
      if (item.id == 9) {
        item.cantidad = double.parse(
            (currentPiscina.metroCubico * 0.002).toStringAsFixed(3));
      }
    }
    setState(() {
      recalcular = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(17, 167, 174, 1),
        title: Text("Inicio"),
        centerTitle: true,
      ),
      drawer: MenuPage(),
      body: Container(
        width: double.infinity,
        height: double.infinity,
       
        child: Container(
          margin: EdgeInsets.all(10),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(
                  height: 10,
                ),
                //usoDeLaAlberca(),
                seleccioneAlberca(),
                SizedBox(
                  height: 20,
                ),
                sinPiscinas
                    ? Column(
                        children: [
                          filtro(),
                          SizedBox(
                            height: 20,
                          ),
                          tabla(),
                        ],
                      )
                    : Container(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget botonRegistroPiscina(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(5, 10, 5, 2),
      child: ButtonTheme(
        minWidth: double.infinity,
        child: RaisedButton(
          // padding: EdgeInsets.all(),
          onPressed: () {
            Navigator.popAndPushNamed(context, "/piscina");
          },
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(7.0)),
          elevation: 0,
          color: Color.fromRGBO(77, 138, 228, 1),
          textColor: Colors.white,
          child: Text(
            'Registrar Piscina',
            style: TextStyle(color: Colors.white),
          ),
        ),
      ),
    );
  }

  Widget usoDeLaAlberca() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Expanded(
          child: Column(
            children: [
              Text(
                "Uso de alberca",
                style: TextStyle(
                    color: Color.fromRGBO(20, 144, 205, 1),
                    fontWeight: FontWeight.bold),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(10, 0, 10, 1),
                margin: EdgeInsets.all(5),
                child: FlutterSwitch(
                  width: 145,
                  height: 51,
                  valueFontSize: 15,
                  toggleSize: 35,
                  activeTextColor: Colors.white,
                  inactiveTextColor: Colors.white,
                  value: status,
                  borderRadius: 30,
                  padding: 7,
                  showOnOff: true,
                  activeColor: Color.fromRGBO(17, 167, 174, 1),
                  activeText: "Residencia",
                  inactiveText: "Comercial",
                  onToggle: (value) {
                    setState(() {
                      status = value;
                    });
                  },
                ),
              ),
            ],
          ),
        ),
        Expanded(
          child: Column(
            children: [
              Text(
                "Medida de la alberca",
                style: TextStyle(
                    color: Color.fromRGBO(20, 144, 205, 1),
                    fontWeight: FontWeight.bold),
              ),
              Container(
                  margin: EdgeInsets.all(5),
                  padding: EdgeInsets.fromLTRB(20, 0, 10, 1),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(25),
                      color: Colors.white,
                      border:
                          Border.all(color: Color.fromRGBO(17, 167, 174, 1))),
                  child: TextField(
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: "Ingrese cantidad",
                        hintStyle: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Color.fromRGBO(0, 0, 0, 160))),
                  ))
            ],
          ),
        )
      ],
    );
  }

  Widget seleccioneAlberca() {
    return Column(
      children: [
        Text("Seleccione su piscina",
            style: TextStyle(
                color: Color.fromRGBO(96, 100, 98, 1),
                fontWeight: FontWeight.w500,
                fontSize: 20)),
        Container(
            margin: EdgeInsets.fromLTRB(5, 10, 5, 5),
            decoration: BoxDecoration(
              color: Color.fromRGBO(255, 255, 255, 1),
              borderRadius: BorderRadius.circular(7),
              border: Border.all(color: Color.fromRGBO(220, 220, 220, 1)),
            ),
            child: FutureBuilder<List<Piscina>>(
              future: piscinasAll,
              builder: (context, data) {
                if (data.connectionState == ConnectionState.done &&
                    data.data.isNotEmpty) {
                  return Container(
                      child: SearchChoices.single(
                    closeButton: 'Cerrar',
                    isExpanded: true,
                    underline: SizedBox(),
                    value: currentPiscina,
                    items: data.data
                        .map((piscina) => DropdownMenuItem<Piscina>(
                              child: Column(
                                children: <Widget>[
                                  Text(
                                    "Nombre: ${piscina.nombre}-M3: ${piscina.metroCubico}",
                                    overflow: TextOverflow.fade,
                                    softWrap: false,
                                    style: TextStyle(
                                        fontWeight: FontWeight.w600,
                                        color: Colors.black87),
                                  ),
                                ],
                              ),
                              value: piscina,
                            ))
                        .toList(),
                    onChanged: (value) {
                      setState(() {
                        if (value != null) {
                          currentPiscina = value;
                          sinPiscinas = true;
                          calcularDatosQuimicos();
                        } else {
                          setState(() {
                            sinPiscinas = false;
                            currentPiscina = null;
                          });
                        }
                      });
                    },
                    hint: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          'Piscinas',
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              color: Colors.black38,
                              fontSize: 16),
                          textAlign: TextAlign.justify,
                        ),
                      ],
                    ),
                  ));
                }
                return Column(
                  children: [
                    Container(
                      child: Center(
                        child: Text("No  Cuenta con Piscinas registradas"),
                      ),
                    ),
                    botonRegistroPiscina(context),
                  ],
                );
              },
            )),
      ],
    );
  }

  Widget filtro() {
    return Card(
      child: Container(
        margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: 550,
              height: 30,
              //color: Color.fromRGBO(20, 144, 205, 1),
              decoration: new BoxDecoration(
                  color: Color.fromRGBO(15, 159, 166, 1),
                  borderRadius: new BorderRadius.only(
                      bottomLeft: const Radius.circular(20),
                      bottomRight: const Radius.circular(20),
                      topLeft: const Radius.circular(20.0),
                      topRight: const Radius.circular(20.0))),
              child: Center(
                child: Text(
                  "Sistema de Filtración",
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 15),
                ),
              ),
            ),
            Column(
              children: [
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Text(
                      "Filtro:",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    Expanded(
                        child: Text(
                      currentPiscina.filtrado,
                      textAlign: TextAlign.center,
                    )),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Text(
                      "Bomba:",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    Expanded(
                        child: Text(
                      currentPiscina.bomba,
                      textAlign: TextAlign.center,
                    )),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget tabla() {
    return recalcular == false
        ? Card(
            child: Container(
              margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: Column(
                children: [
                  Container(
                    width: 550,
                    height: 30,
                    decoration: new BoxDecoration(
                        color: Color.fromRGBO(15, 159, 166, 1),
                        borderRadius: new BorderRadius.only(
                            bottomLeft: const Radius.circular(20),
                            bottomRight: const Radius.circular(20),
                            topLeft: const Radius.circular(20.0),
                            topRight: const Radius.circular(20.0))),
                    //color: Color.fromRGBO(20, 144, 205, 1),
                    child: Center(
                      child: Text(
                        "Quimicos",
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 15),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Column(
                    children: quimico
                        .map((e) => GestureDetector(
                              onTap: () {
                                setState(() {
                                  e.mostraInfo = !e.mostraInfo;
                                });
                              },
                              child: Card(
                                child: Column(
                                  children: [
                                    ListTile(
                                      trailing: e.mostraInfo == false
                                          ? Icon(Icons.keyboard_arrow_down)
                                          : Icon(Icons.keyboard_arrow_up),
                                      title: Wrap(
                                        children: [
                                          Text(
                                            "Nombre: ",
                                            style: TextStyle(
                                                fontWeight: FontWeight.w500,
                                                fontSize: 16),
                                          ),
                                          Text("${e.nombre}",
                                              style: TextStyle(
                                                  //fontWeight: FontWeight.w500,
                                                  fontSize: 16)),
                                        ],
                                      ),
                                      subtitle:Container(
                                        child:Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: [
                                              Text(
                                                  "Cantidad: ${e.cantidad}",
                                                  style: TextStyle(
                                                      color: Color.fromRGBO(
                                                          96, 100, 98, 1),
                                                      fontWeight: FontWeight.w500,
                                                      fontSize: 16)),
                                                      Text(
                                                  "Unidad: ${e.unidad}",
                                                  style: TextStyle(
                                                      color: Color.fromRGBO(
                                                          96, 100, 98, 1),
                                                      fontWeight: FontWeight.w500,
                                                      fontSize: 16)),
                                            
                                     
                                          ],
                                        ), 
                                        )
                                       
                                    ),
                                    e.mostraInfo
                                        ? Wrap(
                                          crossAxisAlignment: WrapCrossAlignment.start,
                                          
                                        children: [
                                          Text("Dosificación: ",
                                              style: TextStyle(
                                                      color: Color.fromRGBO(
                                                          96, 100, 98, 1),
                                                      fontWeight: FontWeight.w500,
                                                      fontSize: 16),textAlign: TextAlign.start,),
                                          Text(
                                            "${e.dosificacion}",
                                            style: TextStyle(
                                                      color: Color.fromRGBO(
                                                          96, 100, 98, 1),
                                                      fontWeight: FontWeight.w500,
                                                      fontSize: 16),
                                                      textAlign: TextAlign.start,
                                          ),
                                        ],
                                      )
                                        : Container(),
                                        SizedBox(height: 15,)
                                  ],
                                ),
                              ),
                            ))
                        .toList(),
                  ),
                ],
              ),
            ),
          )
        : Container(
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
  }
}
