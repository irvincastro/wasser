import 'package:flutter/material.dart';
import 'package:grupo_wasser/main.dart';
import 'package:grupo_wasser/src/css/estilos.dart';
import 'package:grupo_wasser/src/datos/sqlite.dart';
import 'package:grupo_wasser/src/model/Piscina.dart';
import 'package:grupo_wasser/src/model/bomba_modelo.dart';
import 'package:grupo_wasser/src/model/filtro_model.dart';
import 'package:grupo_wasser/src/widget/menu.dart';
import 'package:search_choices/search_choices.dart';
import 'package:grupo_wasser/src/datos/firebaseAut.dart';
import 'package:intl/intl.dart';

class PiscinaPage extends StatefulWidget {
  @override
  _PiscinaPageState createState() => _PiscinaPageState();
}

class _PiscinaPageState extends State<PiscinaPage> {
  int radioValue = 0;
  double area;
  double radio = 0.0;
  double metroCuadrado = 0.0;
  Future<List<Filtro>> filtros;
  Future<List<Bomba>> bombas;

  double litros = 0.0;
  double profundidad = 0.0;
  String nombre = "Circular";
  TextEditingController filtro = TextEditingController();
  TextEditingController bomba = TextEditingController();

  var grupo;
  String imagen = "assets/circulo1.png";
  var value;
  int currentTipoPiscina;
  ////////////////////////
  double longitud = 0.0;
  double diametroMin = 0.0;
  double diametroMax = 0.0;
  double profundidadMax = 0.0;
  double profundidadMin = 0.0;
  Filtro currentFiltro;
  Bomba currentBomba;
  TextEditingController nombrePiscinaText = TextEditingController();
  //cuadrado
  double longitudC = 0.0;
  double ancho = 0.0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    obtenerFiltros();
  }

  obtenerFiltros() {
    filtros = getFiltro();
    bombas = getBomba();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(15, 159, 166, 1),
        centerTitle: true,
        title: Text("Piscina"),
      ),
      drawer: MenuPage(),
      body: Container(
        width: double.infinity,
        height: double.infinity,
        child: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.all(10),
            child: Column(
              children: [
                //color: Colors.white,
                Column(
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: Card(
                            child: Column(
                              children: [
                                ListTile(
                                  contentPadding: EdgeInsets.zero,
                                  title: Text("Circular"),
                                  leading: Radio(
                                    value: 0,
                                    groupValue: radioValue,
                                    activeColor:
                                        Color.fromRGBO(15, 159, 166, 1),
                                    onChanged: (value) {
                                      print(value);
                                      setState(() {
                                        radioValue = value;
                                        imagen = "assets/circulo1.png";
                                        currentTipoPiscina = 1;
                                        nombre = "Circular";
                                      });
                                    },
                                  ),
                                ),
                                ListTile(
                                  contentPadding: EdgeInsets.zero,
                                  title: Text("Oblonga"),
                                  leading: Radio(
                                    activeColor:
                                        Color.fromRGBO(15, 159, 166, 1),
                                    value: 1,
                                    groupValue: radioValue,
                                    onChanged: (value) {
                                      print(value);
                                      setState(() {
                                        radioValue = value;
                                        imagen = "assets/onblong1.png";
                                        currentTipoPiscina = 2;
                                        nombre = "Oblonga";
                                      });
                                    },
                                  ),
                                ),
                                ListTile(
                                  contentPadding: EdgeInsets.zero,
                                  title: Text("Cuadrada"),
                                  leading: Radio(
                                    activeColor:
                                        Color.fromRGBO(15, 159, 166, 1),
                                    value: 2,
                                    groupValue: radioValue,
                                    onChanged: (value) {
                                      print(value);
                                      setState(() {
                                        radioValue = value;
                                        imagen = "assets/cuadrado1.png";
                                        currentTipoPiscina = 3;
                                        nombre = "Cuadrada";
                                      });
                                    },
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            margin: EdgeInsets.all(10),
                            child: Image(
                              image: AssetImage(imagen),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        nombrePiscina(),
                        FutureBuilder<List<Filtro>>(
                          future: filtros,
                          builder: (context, data) {
                            if (data.connectionState == ConnectionState.done &&
                                data.data.isNotEmpty) {
                              return Container(
                                  margin: EdgeInsets.fromLTRB(5, 10, 5, 5),
                                  decoration: boxDecorationselect,
                                  child: Listener(
                                      onPointerDown: (_) =>
                                          FocusScope.of(context).unfocus(),
                                      child: SearchChoices.single(
                                        searchHint: 'Filtrar filtro',
                                        closeButton: "Cerrar",
                                        isExpanded: true,
                                        underline: SizedBox(),
                                        value: currentFiltro,
                                        items: data.data
                                            .map((filtro) =>
                                                DropdownMenuItem<Filtro>(
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: <Widget>[
                                                      Expanded(
                                                          child: Text(
                                                        filtro.nombre,
                                                        overflow:
                                                            TextOverflow.fade,
                                                      )),
                                                    ],
                                                  ),
                                                  value: filtro,
                                                ))
                                            .toList(),
                                        onChanged: (Filtro filtro) async {
                                          if (filtro != null) {
                                            setState(() {
                                              currentFiltro = filtro;
                                            });
                                          } else {
                                            currentFiltro = null;
                                          }
                                        },
                                        hint: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Text(
                                              "Seleccione un filtro",
                                              style: estiloSelectTexto,
                                              textAlign: TextAlign.justify,
                                            ),
                                          ],
                                        ),
                                      )));
                            }
                            return CircularProgressIndicator();
                          },
                        ),
                        FutureBuilder<List<Bomba>>(
                          future: bombas,
                          builder: (context, data) {
                            if (data.connectionState == ConnectionState.done &&
                                data.data.isNotEmpty) {
                              return Container(
                                  margin: EdgeInsets.fromLTRB(5, 10, 5, 5),
                                  decoration: boxDecorationselect,
                                  child: Listener(
                                      onPointerDown: (_) =>
                                          FocusScope.of(context).unfocus(),
                                      child: SearchChoices.single(
                                        searchHint: 'Filtrar bomba',
                                        closeButton: "Cerrar",
                                        isExpanded: true,
                                        underline: SizedBox(),
                                        value: currentBomba,
                                        items: data.data
                                            .map((filtro) =>
                                                DropdownMenuItem<Bomba>(
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: <Widget>[
                                                      Expanded(
                                                          child: Text(
                                                        filtro.nombre,
                                                        overflow:
                                                            TextOverflow.fade,
                                                      )),
                                                    ],
                                                  ),
                                                  value: filtro,
                                                ))
                                            .toList(),
                                        onChanged: (Bomba filtro) async {
                                          if (filtro != null) {
                                            setState(() {
                                              currentBomba = filtro;
                                            });
                                          } else {
                                            currentBomba = null;
                                          }
                                        },
                                        hint: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Text(
                                              "Seleccione una Bomba de agua",
                                              style: estiloSelectTexto,
                                              textAlign: TextAlign.justify,
                                            ),
                                          ],
                                        ),
                                      )));
                            }
                            return CircularProgressIndicator();
                          },
                        ),
                      ],
                    ),
                    radioValue == 0
                        ? Column(
                            children: [
                              Card(
                                child: Container(
                                  child: ListTile(
                                    contentPadding:
                                        EdgeInsets.symmetric(horizontal: 10),
                                    leading: Text("Radio:"),
                                    title: Text(radio.toInt().toString()),
                                    trailing: Container(
                                      child: Wrap(
                                        spacing: -40,
                                        children: [
                                          FlatButton(
                                            padding:
                                                EdgeInsets.fromLTRB(0, 0, 0, 0),
                                            child: Container(
                                              padding: EdgeInsets.fromLTRB(
                                                  5, 5, 5, 5),
                                              decoration: BoxDecoration(
                                                  color: Color.fromRGBO(
                                                      255, 255, 255, 1),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          100),
                                                  boxShadow: [
                                                    BoxShadow(
                                                      color: Colors.grey
                                                          .withOpacity(1),
                                                      blurRadius: 3.0,
                                                      spreadRadius: 0,
                                                      offset: Offset(
                                                        0.0,
                                                        0.0,
                                                      ),
                                                    )
                                                  ]),
                                              child: Icon(
                                                Icons.add,
                                                size: 30,
                                                color: Color.fromRGBO(
                                                    15, 159, 166, 1),
                                              ),
                                            ),
                                            onPressed: () {
                                              if (radio < 20) {
                                                setState(() {
                                                  radio = radio + 1;
                                                  calcularArea(radioValue);
                                                });
                                              }
                                            },
                                          ),
                                          FlatButton(
                                            padding:
                                                EdgeInsets.fromLTRB(0, 0, 0, 0),
                                            child: Container(
                                              padding: EdgeInsets.fromLTRB(
                                                  5, 5, 5, 5),
                                              decoration: BoxDecoration(
                                                  color: Color.fromRGBO(
                                                      255, 255, 255, 1),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          100),
                                                  boxShadow: [
                                                    BoxShadow(
                                                      color: Colors.grey
                                                          .withOpacity(1),
                                                      blurRadius: 3.0,
                                                      spreadRadius: 0,
                                                      offset: Offset(
                                                        0.0,
                                                        0.0,
                                                      ),
                                                    )
                                                  ]),
                                              child: Icon(
                                                Icons.remove,
                                                size: 30,
                                                color: Color.fromRGBO(
                                                    15, 159, 166, 1),
                                              ),
                                            ),
                                            onPressed: () {
                                              if (radio > 1) {
                                                setState(() {
                                                  radio = radio - 1;
                                                  calcularArea(radioValue);
                                                });
                                              }
                                            },
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Card(
                                child: Container(
                                  child: ListTile(
                                    contentPadding:
                                        EdgeInsets.symmetric(horizontal: 10),
                                    leading: Text("Profundidad:"),
                                    title: Text(profundidad.toInt().toString()),
                                    trailing: Container(
                                      child: Wrap(
                                        spacing: -40,
                                        children: [
                                          FlatButton(
                                            padding:
                                                EdgeInsets.fromLTRB(0, 0, 0, 0),
                                            child: Container(
                                              padding: EdgeInsets.fromLTRB(
                                                  5, 5, 5, 5),
                                              decoration: BoxDecoration(
                                                  color: Color.fromRGBO(
                                                      255, 255, 255, 1),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          100),
                                                  boxShadow: [
                                                    BoxShadow(
                                                      color: Colors.grey
                                                          .withOpacity(1),
                                                      blurRadius: 3.0,
                                                      spreadRadius: 0,
                                                      offset: Offset(
                                                        0.0,
                                                        0.0,
                                                      ),
                                                    )
                                                  ]),
                                              child: Icon(
                                                Icons.add,
                                                size: 30,
                                                color: Color.fromRGBO(
                                                    15, 159, 166, 1),
                                              ),
                                            ),
                                            onPressed: () {
                                              if (profundidad < 18) {
                                                setState(() {
                                                  profundidad = profundidad + 1;
                                                  calcularArea(radioValue);
                                                });
                                              }
                                            },
                                          ),
                                          FlatButton(
                                            padding:
                                                EdgeInsets.fromLTRB(0, 0, 0, 0),
                                            child: Container(
                                              padding: EdgeInsets.fromLTRB(
                                                  5, 5, 5, 5),
                                              decoration: BoxDecoration(
                                                  color: Color.fromRGBO(
                                                      255, 255, 255, 1),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          100),
                                                  boxShadow: [
                                                    BoxShadow(
                                                      color: Colors.grey
                                                          .withOpacity(1),
                                                      blurRadius: 3.0,
                                                      spreadRadius: 0,
                                                      offset: Offset(
                                                        0.0,
                                                        0.0,
                                                      ),
                                                    )
                                                  ]),
                                              child: Icon(
                                                Icons.remove,
                                                size: 30,
                                                color: Color.fromRGBO(
                                                    15, 159, 166, 1),
                                              ),
                                            ),
                                            onPressed: () {
                                              if (profundidad > 1) {
                                                setState(() {
                                                  profundidad = profundidad - 1;
                                                  calcularArea(radioValue);
                                                });
                                              }
                                            },
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Card(
                                elevation: 0,
                                child: Column(
                                  children: [
                                    Container(
                                      height: 30,
                                      child: Center(
                                        child: Wrap(
                                          children: [
                                            Text("Metros cúbicos: ",
                                                style: TextStyle(
                                                    color: Colors.grey[700],
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 20)),
                                            Text(
                                                '${NumberFormat.currency(decimalDigits: 2, symbol: '').format(metroCuadrado)}',
                                                style: TextStyle(
                                                    color: Colors.grey[700],
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 20))
                                          ],
                                        ),
                                      ),
                                    ),
                                    Container(
                                      height: 30,
                                      child: Center(
                                        child: Wrap(
                                          children: [
                                            Text(
                                                "Litros: ",
                                                style: TextStyle(
                                                    color: Colors.grey[700],
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 20)),
                                            Text(
                                                "${NumberFormat.currency(decimalDigits: 2, symbol: '').format(litros)}",
                                                style: TextStyle(
                                                    color: Colors.grey[700],
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 20)),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          )
                        : Container(),
                    radioValue == 1
                        ? Column(
                            children: [
                              Card(
                                child: Container(
                                  child: ListTile(
                                    contentPadding:
                                        EdgeInsets.symmetric(horizontal: 5),
                                    leading: Text("Longitud:"),
                                    title: Text(longitud.toInt().toString()),
                                    trailing: Container(
                                      child: Wrap(
                                        spacing: -40,
                                        children: [
                                          FlatButton(
                                            padding:
                                                EdgeInsets.fromLTRB(0, 0, 0, 0),
                                            child: Container(
                                              padding: EdgeInsets.fromLTRB(
                                                  5, 5, 5, 5),
                                              decoration: BoxDecoration(
                                                  color: Color.fromRGBO(
                                                      255, 255, 255, 1),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          100),
                                                  boxShadow: [
                                                    BoxShadow(
                                                      color: Colors.grey
                                                          .withOpacity(1),
                                                      blurRadius: 3.0,
                                                      spreadRadius: 0,
                                                      offset: Offset(
                                                        0.0,
                                                        0.0,
                                                      ),
                                                    )
                                                  ]),
                                              child: Icon(
                                                Icons.add,
                                                size: 30,
                                                color: Color.fromRGBO(
                                                    15, 159, 166, 1),
                                              ),
                                            ),
                                            onPressed: () {
                                              if (longitud < 100) {
                                                setState(() {
                                                  longitud = longitud + 1;
                                                  calcularArea(radioValue);
                                                });
                                              }
                                            },
                                          ),
                                          FlatButton(
                                            padding:
                                                EdgeInsets.fromLTRB(0, 0, 0, 0),
                                            child: Container(
                                              padding: EdgeInsets.fromLTRB(
                                                  5, 5, 5, 5),
                                              decoration: BoxDecoration(
                                                  color: Color.fromRGBO(
                                                      255, 255, 255, 1),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          100),
                                                  boxShadow: [
                                                    BoxShadow(
                                                      color: Colors.grey
                                                          .withOpacity(1),
                                                      blurRadius: 3.0,
                                                      spreadRadius: 0,
                                                      offset: Offset(
                                                        0.0,
                                                        0.0,
                                                      ),
                                                    )
                                                  ]),
                                              child: Icon(
                                                Icons.remove,
                                                size: 30,
                                                color: Color.fromRGBO(
                                                    15, 159, 166, 1),
                                              ),
                                            ),
                                            onPressed: () {
                                              if (longitud > 1) {
                                                setState(() {
                                                  longitud = longitud - 1;
                                                  calcularArea(radioValue);
                                                });
                                              }
                                            },
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Card(
                                child: Container(
                                  child: ListTile(
                                    contentPadding:
                                        EdgeInsets.symmetric(horizontal: 5),
                                    leading: Text("Diámetro Max:"),
                                    title: Text(diametroMax.toInt().toString()),
                                    trailing: Container(
                                      child: Wrap(
                                        spacing: -40,
                                        children: [
                                          FlatButton(
                                            padding:
                                                EdgeInsets.fromLTRB(0, 0, 0, 0),
                                            child: Container(
                                              padding: EdgeInsets.fromLTRB(
                                                  5, 5, 5, 5),
                                              decoration: BoxDecoration(
                                                  color: Color.fromRGBO(
                                                      255, 255, 255, 1),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          100),
                                                  boxShadow: [
                                                    BoxShadow(
                                                      color: Colors.grey
                                                          .withOpacity(1),
                                                      blurRadius: 3.0,
                                                      spreadRadius: 0,
                                                      offset: Offset(
                                                        0.0,
                                                        0.0,
                                                      ),
                                                    )
                                                  ]),
                                              child: Icon(
                                                Icons.add,
                                                size: 30,
                                                color: Color.fromRGBO(
                                                    15, 159, 166, 1),
                                              ),
                                            ),
                                            onPressed: () {
                                              if (diametroMax < 40) {
                                                setState(() {
                                                  diametroMax = diametroMax + 1;
                                                  calcularArea(radioValue);
                                                });
                                              }
                                            },
                                          ),
                                          FlatButton(
                                            padding:
                                                EdgeInsets.fromLTRB(0, 0, 0, 0),
                                            child: Container(
                                              padding: EdgeInsets.fromLTRB(
                                                  5, 5, 5, 5),
                                              decoration: BoxDecoration(
                                                  color: Color.fromRGBO(
                                                      255, 255, 255, 1),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          100),
                                                  boxShadow: [
                                                    BoxShadow(
                                                      color: Colors.grey
                                                          .withOpacity(1),
                                                      blurRadius: 3.0,
                                                      spreadRadius: 0,
                                                      offset: Offset(
                                                        0.0,
                                                        0.0,
                                                      ),
                                                    )
                                                  ]),
                                              child: Icon(
                                                Icons.remove,
                                                size: 30,
                                                color: Color.fromRGBO(
                                                    15, 159, 166, 1),
                                              ),
                                            ),
                                            onPressed: () {
                                              if (diametroMax > 1) {
                                                setState(() {
                                                  diametroMax = diametroMax - 1;
                                                  calcularArea(radioValue);
                                                });
                                              }
                                            },
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Card(
                                child: Container(
                                  child: ListTile(
                                    contentPadding:
                                        EdgeInsets.symmetric(horizontal: 5),
                                    leading: Text("Diámetro Min:"),
                                    title: Text(diametroMin.toInt().toString()),
                                    trailing: Container(
                                      child: Wrap(
                                        spacing: -40,
                                        children: [
                                          FlatButton(
                                            padding:
                                                EdgeInsets.fromLTRB(0, 0, 0, 0),
                                            child: Container(
                                              padding: EdgeInsets.fromLTRB(
                                                  5, 5, 5, 5),
                                              decoration: BoxDecoration(
                                                  color: Color.fromRGBO(
                                                      255, 255, 255, 1),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          100),
                                                  boxShadow: [
                                                    BoxShadow(
                                                      color: Colors.grey
                                                          .withOpacity(1),
                                                      blurRadius: 3.0,
                                                      spreadRadius: 0,
                                                      offset: Offset(
                                                        0.0,
                                                        0.0,
                                                      ),
                                                    )
                                                  ]),
                                              child: Icon(
                                                Icons.add,
                                                size: 30,
                                                color: Color.fromRGBO(
                                                    15, 159, 166, 1),
                                              ),
                                            ),
                                            onPressed: () {
                                              if (diametroMin < 40) {
                                                setState(() {
                                                  diametroMin = diametroMin + 1;
                                                  calcularArea(radioValue);
                                                });
                                              }
                                            },
                                          ),
                                          FlatButton(
                                            padding:
                                                EdgeInsets.fromLTRB(0, 0, 0, 0),
                                            child: Container(
                                              padding: EdgeInsets.fromLTRB(
                                                  5, 5, 5, 5),
                                              decoration: BoxDecoration(
                                                  color: Color.fromRGBO(
                                                      255, 255, 255, 1),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          100),
                                                  boxShadow: [
                                                    BoxShadow(
                                                      color: Colors.grey
                                                          .withOpacity(1),
                                                      blurRadius: 3.0,
                                                      spreadRadius: 0,
                                                      offset: Offset(
                                                        0.0,
                                                        0.0,
                                                      ),
                                                    )
                                                  ]),
                                              child: Icon(
                                                Icons.remove,
                                                size: 30,
                                                color: Color.fromRGBO(
                                                    15, 159, 166, 1),
                                              ),
                                            ),
                                            onPressed: () {
                                              if (diametroMin > 1) {
                                                setState(() {
                                                  diametroMin = diametroMin - 1;
                                                  calcularArea(radioValue);
                                                });
                                              }
                                            },
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Card(
                                child: Container(
                                  child: ListTile(
                                    contentPadding:
                                        EdgeInsets.symmetric(horizontal: 5),
                                    leading: Text("Profundidad Max:"),
                                    title:
                                        Text(profundidadMax.toInt().toString()),
                                    trailing: Container(
                                      width: size.width * .4,
                                      child: Wrap(
                                        spacing: -40,
                                        children: [
                                          FlatButton(
                                            padding:
                                                EdgeInsets.fromLTRB(0, 0, 0, 0),
                                            child: Container(
                                              padding: EdgeInsets.fromLTRB(
                                                  5, 5, 5, 5),
                                              decoration: BoxDecoration(
                                                  color: Color.fromRGBO(
                                                      255, 255, 255, 1),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          100),
                                                  boxShadow: [
                                                    BoxShadow(
                                                      color: Colors.grey
                                                          .withOpacity(1),
                                                      blurRadius: 3.0,
                                                      spreadRadius: 0,
                                                      offset: Offset(
                                                        0.0,
                                                        0.0,
                                                      ),
                                                    )
                                                  ]),
                                              child: Icon(
                                                Icons.add,
                                                size: 30,
                                                color: Color.fromRGBO(
                                                    15, 159, 166, 1),
                                              ),
                                            ),
                                            onPressed: () {
                                              if (profundidadMax < 18) {
                                                setState(() {
                                                  profundidadMax =
                                                      profundidadMax + 1;
                                                  calcularArea(radioValue);
                                                });
                                              }
                                            },
                                          ),
                                          FlatButton(
                                            padding:
                                                EdgeInsets.fromLTRB(0, 0, 0, 0),
                                            child: Container(
                                              padding: EdgeInsets.fromLTRB(
                                                  5, 5, 5, 5),
                                              decoration: BoxDecoration(
                                                  color: Color.fromRGBO(
                                                      255, 255, 255, 1),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          100),
                                                  boxShadow: [
                                                    BoxShadow(
                                                      color: Colors.grey
                                                          .withOpacity(1),
                                                      blurRadius: 3.0,
                                                      spreadRadius: 0,
                                                      offset: Offset(
                                                        0.0,
                                                        0.0,
                                                      ),
                                                    )
                                                  ]),
                                              child: Icon(
                                                Icons.remove,
                                                size: 30,
                                                color: Color.fromRGBO(
                                                    15, 159, 166, 1),
                                              ),
                                            ),
                                            onPressed: () {
                                              if (profundidadMax > 1) {
                                                setState(() {
                                                  profundidadMax =
                                                      profundidadMax - 1;
                                                  calcularArea(radioValue);
                                                });
                                              }
                                            },
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Card(
                                child: Container(
                                  child: ListTile(
                                    contentPadding:
                                        EdgeInsets.symmetric(horizontal: 5),
                                    leading: Text("Profundidad Min:"),
                                    title:
                                        Text(profundidadMin.toInt().toString()),
                                    trailing: Container(
                                      child: Wrap(
                                        spacing: -40,
                                        children: [
                                          FlatButton(
                                            padding:
                                                EdgeInsets.fromLTRB(0, 0, 0, 0),
                                            child: Container(
                                              padding: EdgeInsets.fromLTRB(
                                                  5, 5, 5, 5),
                                              decoration: BoxDecoration(
                                                  color: Color.fromRGBO(
                                                      255, 255, 255, 1),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          100),
                                                  boxShadow: [
                                                    BoxShadow(
                                                      color: Colors.grey
                                                          .withOpacity(1),
                                                      blurRadius: 3.0,
                                                      spreadRadius: 0,
                                                      offset: Offset(
                                                        0.0,
                                                        0.0,
                                                      ),
                                                    )
                                                  ]),
                                              child: Icon(
                                                Icons.add,
                                                size: 30,
                                                color: Color.fromRGBO(
                                                    15, 159, 166, 1),
                                              ),
                                            ),
                                            onPressed: () {
                                              if (profundidadMin < 18) {
                                                setState(() {
                                                  profundidadMin =
                                                      profundidadMin + 1;
                                                  calcularArea(radioValue);
                                                });
                                              }
                                            },
                                          ),
                                          FlatButton(
                                            padding:
                                                EdgeInsets.fromLTRB(0, 0, 0, 0),
                                            child: Container(
                                              padding: EdgeInsets.fromLTRB(
                                                  5, 5, 5, 5),
                                              decoration: BoxDecoration(
                                                  color: Color.fromRGBO(
                                                      255, 255, 255, 1),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          100),
                                                  boxShadow: [
                                                    BoxShadow(
                                                      color: Colors.grey
                                                          .withOpacity(1),
                                                      blurRadius: 3.0,
                                                      spreadRadius: 0,
                                                      offset: Offset(
                                                        0.0,
                                                        0.0,
                                                      ),
                                                    )
                                                  ]),
                                              child: Icon(
                                                Icons.remove,
                                                size: 30,
                                                color: Color.fromRGBO(
                                                    15, 159, 166, 1),
                                              ),
                                            ),
                                            onPressed: () {
                                              if (profundidadMin > 1) {
                                                setState(() {
                                                  profundidadMin =
                                                      profundidadMin - 1;
                                                  calcularArea(radioValue);
                                                });
                                              }
                                            },
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Card(
                                elevation: 0,
                                child: Column(
                                  children: [
                                    Container(
                                      height: 30,
                                      child: Center(
                                        child: Wrap(
                                          children: [
                                            Text("Metros cúbicos: ",
                                                style: TextStyle(
                                                    color: Colors.grey[700],
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 20)),
                                            Text(
                                                '${NumberFormat.currency(decimalDigits: 2, symbol: '').format(metroCuadrado)}',
                                                style: TextStyle(
                                                    color: Colors.grey[700],
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 20))
                                          ],
                                        ),
                                      ),
                                    ),
                                    Container(
                                      height: 30,
                                      child: Center(
                                        child: Wrap(
                                          children: [
                                            Text(
                                                "Litros: ",
                                                style: TextStyle(
                                                    color: Colors.grey[700],
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 20)),
                                            Text(
                                                "${NumberFormat.currency(decimalDigits: 2, symbol: '').format(litros)}",
                                                style: TextStyle(
                                                    color: Colors.grey[700],
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 20)),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          )
                        : Container(),
                    radioValue == 2
                        ? Column(
                            children: [
                              Card(
                                child: Container(
                                  child: ListTile(
                                    contentPadding:
                                        EdgeInsets.symmetric(horizontal: 10),
                                    leading: Text("Longitud:"),
                                    title: Text(longitudC.toInt().toString()),
                                    trailing: Container(
                                      child: Wrap(
                                        spacing: -40,
                                        children: [
                                          FlatButton(
                                            padding:
                                                EdgeInsets.fromLTRB(0, 0, 0, 0),
                                            child: Container(
                                              padding: EdgeInsets.fromLTRB(
                                                  5, 5, 5, 5),
                                              decoration: BoxDecoration(
                                                  color: Color.fromRGBO(
                                                      255, 255, 255, 1),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          100),
                                                  boxShadow: [
                                                    BoxShadow(
                                                      color: Colors.grey
                                                          .withOpacity(1),
                                                      blurRadius: 3.0,
                                                      spreadRadius: 0,
                                                      offset: Offset(
                                                        0.0,
                                                        0.0,
                                                      ),
                                                    )
                                                  ]),
                                              child: Icon(
                                                Icons.add,
                                                size: 30,
                                                color: Color.fromRGBO(
                                                    15, 159, 166, 1),
                                              ),
                                            ),
                                            onPressed: () {
                                              if (longitudC < 100) {
                                                setState(() {
                                                  longitudC = longitudC + 1;
                                                });
                                              }
                                              calcularArea(radioValue);
                                            },
                                          ),
                                          FlatButton(
                                            padding:
                                                EdgeInsets.fromLTRB(0, 0, 0, 0),
                                            child: Container(
                                              padding: EdgeInsets.fromLTRB(
                                                  5, 5, 5, 5),
                                              decoration: BoxDecoration(
                                                  color: Color.fromRGBO(
                                                      255, 255, 255, 1),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          100),
                                                  boxShadow: [
                                                    BoxShadow(
                                                      color: Colors.grey
                                                          .withOpacity(1),
                                                      blurRadius: 3.0,
                                                      spreadRadius: 0,
                                                      offset: Offset(
                                                        0.0,
                                                        0.0,
                                                      ),
                                                    )
                                                  ]),
                                              child: Icon(
                                                Icons.remove,
                                                size: 30,
                                                color: Color.fromRGBO(
                                                    15, 159, 166, 1),
                                              ),
                                            ),
                                            onPressed: () {
                                              if (longitudC > 1) {
                                                setState(() {
                                                  longitudC = longitudC - 1;
                                                  calcularArea(radioValue);
                                                });
                                              }
                                            },
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Card(
                                child: Container(
                                  child: ListTile(
                                    contentPadding:
                                        EdgeInsets.symmetric(horizontal: 10),
                                    leading: Text("Ancho:"),
                                    title: Text(ancho.toInt().toString()),
                                    trailing: Container(
                                      child: Wrap(
                                        spacing: -40,
                                        children: [
                                          FlatButton(
                                            padding:
                                                EdgeInsets.fromLTRB(0, 0, 0, 0),
                                            child: Container(
                                              padding: EdgeInsets.fromLTRB(
                                                  5, 5, 5, 5),
                                              decoration: BoxDecoration(
                                                  color: Color.fromRGBO(
                                                      255, 255, 255, 1),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          100),
                                                  boxShadow: [
                                                    BoxShadow(
                                                      color: Colors.grey
                                                          .withOpacity(1),
                                                      blurRadius: 3.0,
                                                      spreadRadius: 0,
                                                      offset: Offset(
                                                        0.0,
                                                        0.0,
                                                      ),
                                                    )
                                                  ]),
                                              child: Icon(
                                                Icons.add,
                                                size: 30,
                                                color: Color.fromRGBO(
                                                    15, 159, 166, 1),
                                              ),
                                            ),
                                            onPressed: () {
                                              if (ancho < 40) {
                                                setState(() {
                                                  ancho = ancho + 1;
                                                  calcularArea(radioValue);
                                                });
                                              }
                                            },
                                          ),
                                          FlatButton(
                                            padding:
                                                EdgeInsets.fromLTRB(0, 0, 0, 0),
                                            child: Container(
                                              padding: EdgeInsets.fromLTRB(
                                                  5, 5, 5, 5),
                                              decoration: BoxDecoration(
                                                  color: Color.fromRGBO(
                                                      255, 255, 255, 1),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          100),
                                                  boxShadow: [
                                                    BoxShadow(
                                                      color: Colors.grey
                                                          .withOpacity(1),
                                                      blurRadius: 3.0,
                                                      spreadRadius: 0,
                                                      offset: Offset(
                                                        0.0,
                                                        0.0,
                                                      ),
                                                    )
                                                  ]),
                                              child: Icon(
                                                Icons.remove,
                                                size: 30,
                                                color: Color.fromRGBO(
                                                    15, 159, 166, 1),
                                              ),
                                            ),
                                            onPressed: () {
                                              if (ancho > 1) {
                                                setState(() {
                                                  ancho = ancho - 1;
                                                  calcularArea(radioValue);
                                                });
                                              }
                                            },
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Card(
                                child: Container(
                                  child: ListTile(
                                    contentPadding:
                                        EdgeInsets.symmetric(horizontal: 10),
                                    leading: Text("Profundidad Min:"),
                                    title:
                                        Text(profundidadMin.toInt().toString()),
                                    trailing: Container(
                                      child: Wrap(
                                        spacing: -40,
                                        children: [
                                          FlatButton(
                                            padding:
                                                EdgeInsets.fromLTRB(0, 0, 0, 0),
                                            child: Container(
                                              padding: EdgeInsets.fromLTRB(
                                                  5, 5, 5, 5),
                                              decoration: BoxDecoration(
                                                  color: Color.fromRGBO(
                                                      255, 255, 255, 1),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          100),
                                                  boxShadow: [
                                                    BoxShadow(
                                                      color: Colors.grey
                                                          .withOpacity(1),
                                                      blurRadius: 3.0,
                                                      spreadRadius: 0,
                                                      offset: Offset(
                                                        0.0,
                                                        0.0,
                                                      ),
                                                    )
                                                  ]),
                                              child: Icon(
                                                Icons.add,
                                                size: 30,
                                                color: Color.fromRGBO(
                                                    15, 159, 166, 1),
                                              ),
                                            ),
                                            onPressed: () {
                                              if (profundidadMin < 18) {
                                                setState(() {
                                                  profundidadMin =
                                                      profundidadMin + 1;
                                                  calcularArea(radioValue);
                                                });
                                              }
                                            },
                                          ),
                                          FlatButton(
                                            padding:
                                                EdgeInsets.fromLTRB(0, 0, 0, 0),
                                            child: Container(
                                              padding: EdgeInsets.fromLTRB(
                                                  5, 5, 5, 5),
                                              decoration: BoxDecoration(
                                                  color: Color.fromRGBO(
                                                      255, 255, 255, 1),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          100),
                                                  boxShadow: [
                                                    BoxShadow(
                                                      color: Colors.grey
                                                          .withOpacity(1),
                                                      blurRadius: 3.0,
                                                      spreadRadius: 0,
                                                      offset: Offset(
                                                        0.0,
                                                        0.0,
                                                      ),
                                                    )
                                                  ]),
                                              child: Icon(
                                                Icons.remove,
                                                size: 30,
                                                color: Color.fromRGBO(
                                                    15, 159, 166, 1),
                                              ),
                                            ),
                                            onPressed: () {
                                              if (profundidadMin > 1) {
                                                setState(() {
                                                  profundidadMin =
                                                      profundidadMin - 1;
                                                  calcularArea(radioValue);
                                                });
                                              }
                                            },
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Card(
                                child: Container(
                                  child: ListTile(
                                    contentPadding:
                                        EdgeInsets.symmetric(horizontal: 10),
                                    leading: Text("Profundidad Max:"),
                                    title:
                                        Text(profundidadMax.toInt().toString()),
                                    trailing: Container(
                                      child: Wrap(
                                        spacing: -40,
                                        children: [
                                          FlatButton(
                                            padding:
                                                EdgeInsets.fromLTRB(0, 0, 0, 0),
                                            child: Container(
                                              padding: EdgeInsets.fromLTRB(
                                                  5, 5, 5, 5),
                                              decoration: BoxDecoration(
                                                  color: Color.fromRGBO(
                                                      255, 255, 255, 1),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          100),
                                                  boxShadow: [
                                                    BoxShadow(
                                                      color: Colors.grey
                                                          .withOpacity(1),
                                                      blurRadius: 3.0,
                                                      spreadRadius: 0,
                                                      offset: Offset(
                                                        0.0,
                                                        0.0,
                                                      ),
                                                    )
                                                  ]),
                                              child: Icon(
                                                Icons.add,
                                                size: 30,
                                                color: Color.fromRGBO(
                                                    15, 159, 166, 1),
                                              ),
                                            ),
                                            onPressed: () {
                                              if (profundidadMax < 18) {
                                                setState(() {
                                                  profundidadMax =
                                                      profundidadMax + 1;
                                                  calcularArea(radioValue);
                                                });
                                              }
                                            },
                                          ),
                                          FlatButton(
                                            padding:
                                                EdgeInsets.fromLTRB(0, 0, 0, 0),
                                            child: Container(
                                              padding: EdgeInsets.fromLTRB(
                                                  5, 5, 5, 5),
                                              decoration: BoxDecoration(
                                                  color: Color.fromRGBO(
                                                      255, 255, 255, 1),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          100),
                                                  boxShadow: [
                                                    BoxShadow(
                                                      color: Colors.grey
                                                          .withOpacity(1),
                                                      blurRadius: 3.0,
                                                      spreadRadius: 0,
                                                      offset: Offset(
                                                        0.0,
                                                        0.0,
                                                      ),
                                                    )
                                                  ]),
                                              child: Icon(
                                                Icons.remove,
                                                size: 30,
                                                color: Color.fromRGBO(
                                                    15, 159, 166, 1),
                                              ),
                                            ),
                                            onPressed: () {
                                              if (profundidadMax > 1) {
                                                setState(() {
                                                  profundidadMax =
                                                      profundidadMax - 1;
                                                  calcularArea(radioValue);
                                                });
                                              }
                                            },
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Card(
                                elevation: 0,
                                child: Column(
                                  children: [
                                    Container(
                                      height: 30,
                                      child: Center(
                                        child: Wrap(
                                          children: [
                                            Text("Metros cúbicos: ",
                                                style: TextStyle(
                                                    color: Colors.grey[700],
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 20)),
                                            Text(
                                                '${NumberFormat.currency(decimalDigits: 2, symbol: '').format(metroCuadrado)}',
                                                style: TextStyle(
                                                    color: Colors.grey[700],
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 20))
                                          ],
                                        ),
                                      ),
                                    ),
                                    Container(
                                      height: 30,
                                      child: Center(
                                        child: Wrap(
                                          children: [
                                            Text(
                                                "Litros: ",
                                                style: TextStyle(
                                                    color: Colors.grey[700],
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 20)),
                                            Text(
                                                "${NumberFormat.currency(decimalDigits: 2, symbol: '').format(litros)}",
                                                style: TextStyle(
                                                    color: Colors.grey[700],
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 20)),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          )
                        : Container(),
                    botonRegistroPiscina(context),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget botonRegistroPiscina(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(5, 10, 5, 2),
      child: ButtonTheme(
        minWidth: double.infinity,
        child: RaisedButton(
          // padding: EdgeInsets.all(),
          // " Filtro de 28'' c/valvula toop de vias .Cap.Filtar de 18m3/h (200kg) ",
          //     300,currentTipoPiscina , nombre, id, "Motobomba 1.5 HP 110/220v")
          onPressed: () async {
            print(nombre);
            await TaskDatabase.db.addPiscinaToDatabase(Piscina(
                bomba: currentBomba.nombre,
                correo: prefs.correo,
                filtrado: currentFiltro.nombre,
                metroCubico: metroCuadrado,
                nombre: nombrePiscinaText.text,
                tipoAlberca: currentTipoPiscina));
            Navigator.popAndPushNamed(context, "/inicio");
          },
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(7.0)),
          elevation: 0,
          color: Color.fromRGBO(15, 159, 166, 1),
          textColor: Colors.white,
          child: Text(
            'Guardar',
            style: TextStyle(color: Colors.white),
          ),
        ),
      ),
    );
  }

  Widget nombrePiscina() {
    return Container(
      margin: paddingTextFiel,
      child: TextFormField(
        validator: (value) {
          if (value.isEmpty) {
            return 'El campo es obligatorio';
          }
        },
        //textCapitalization: TextCapitalization.characters,
        keyboardType: TextInputType.text,
        controller: nombrePiscinaText,
        style: textStileCampo,
        decoration: InputDecoration(
          filled: true,
          fillColor: fillColorCampo,
          border: borderCampo,
          enabledBorder: enabledBorderCampo,
          focusedBorder: focusedBorderCampo,
          contentPadding: contentPAddingCampo,
          hintText: 'Nombre(s)',
          hintStyle: campoTextoSstileHintText,
        ),
      ),
    );
  }

  calcularArea(int tipoPiscina) {
    if (tipoPiscina == 0) {
      setState(() {
        area = 3.14 * radio * radio;
        area = double.parse(area.toStringAsFixed(2));
        metroCuadrado = area * profundidad;

        litros = metroCuadrado * 1000;
      });
    }
    if (tipoPiscina == 2) {
      setState(() {
        area = (longitudC * ancho);

        metroCuadrado = area * ((profundidadMax + profundidadMin) / 2);
        litros = metroCuadrado * 1000;
      });
      print("es un cuadrado");
    }
    if (tipoPiscina == 1) {
      setState(() {
        area = 0.45 * (diametroMax + diametroMin) * longitud;
        area = double.parse(area.toStringAsFixed(2));
        metroCuadrado = area * ((profundidadMax + profundidadMin) / 2);
        litros = metroCuadrado * 1000;
      });
    }
  }
}
