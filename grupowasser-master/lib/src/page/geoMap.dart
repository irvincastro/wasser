/*
*     En esta vista se maneja todo lo relacionado con la geolocalizacion
*     la vista dedicada a la funcionalidad del cliente
*/

// ------------------------------|>| Importaciones |<|------------------------------
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:grupo_wasser/src/page/storeCard.dart';
import 'package:grupo_wasser/src/widget/menu.dart';
import 'package:provider/provider.dart';
import 'geopoint.dart';
import 'loading.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:math';
import 'package:vector_math/vector_math.dart';
import 'dart:async';

// ------------------------------|>| Constructor |<|------------------------------
class GeoMap extends StatefulWidget {
  @override
  _GeoMapState createState() => _GeoMapState();
}

class _GeoMapState extends State<GeoMap> {
// ------------------------------|>| Variables |<|------------------------------

  // Lista para almacenar los marcadores del mapa
  List<Marker> markers = [];

  // Configuración para desplegar u ocultar el mapa
  bool mapToggle = false;

  // Distancia valida para permitir el despliegue de los marcadores
  final int _validKm = 20;
  // ignore: non_constant_identifier_names
  final double AVERAGE_RADIUS_OF_EARTH_KM = 6371;

  // Controlador usado por el API de google maps
  GoogleMapController mapController;

// |----------------> Seguimiento de la posicion del usuario <----------------|

  // Posicion inicial del usuario al iniciar la app
  Position _initLocation = Position(latitude: 0.0, longitude: 0.0);
  // Posicion actual del usuario
  Position _currentLocation = Position(latitude: 0.0, longitude: 0.0);

// |----------------> Configuraciones del temporizador <----------------|
  bool _timerON = false;
  bool _markersON = true;
  Timer _timer;
  int _start = 1;

  // Ejecucion al inicar la aplicacion
  void initState() {
    super.initState();

    // Se obtiene la posicion actual del usuario
    Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high)
        .then((currloc) {
      setState(() {
        // Almacenamos la ubicacion
        _initLocation = currloc ?? Position(latitude: 0.0, longitude: 0.0);
        _currentLocation = currloc ?? Position(latitude: 0.0, longitude: 0.0);

        // Activamos el renderizado del mapa
        mapToggle = true;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    // Panel de informacion para desplegar la informacion del marcador seleccionado
    // Se muestra el ID, Nombre y Posicion (Latitud y Longitud)
    void _showWorkerPanel(String firstName, String lastName1, String lastName2, String phonenumber,
        GeoPoint suclocation, int rating) {
      showModalBottomSheet(
          context: context,
          builder: (context) {
            return Container(
              padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 60.0),
              child: StoreCard(
                  name: firstName,
                  lastName1: lastName1,
                  lastName2: lastName2,
                  phonenumber: phonenumber,
                  rating: rating,
                  location:
                      GeoPoint(suclocation.latitude, suclocation.longitude)),
            );
          });
    }

    // Provedor de puntos geograficos obtenidos desde Firebase
    final geopoints = Provider.of<List<Geopoint>>(context) ?? [];

// ----------|>| Filtrado de los marcadores |<|----------
    // _markersON es *false* cuando el temporizador se encuentra activo para evitar la continua ejecucion de los calculos
    if (_markersON) {
      markers = [];

      // Para cada marcador obtenido de Firebase...
      for (var point in geopoints) {
        // Se calcula la distancia entre la posicion actual del usuario y la posicion del marcador
        int distance = calculateDistance(
            _currentLocation.latitude,
            _currentLocation.longitude,
            point.location.latitude,
            point.location.longitude);

        // Si la distancia es menor a la distancia valida {_validKM = 20}
        // Se añade el marcador a la lista de marcadores {markers}
        if (distance < _validKm && point.availability == true) {
          markers.add(
            Marker(
              markerId: MarkerId(point.name ?? "IDNotFound"),
              position: LatLng(point.location.latitude ?? 0.0,
                  point.location.longitude ?? 0.0),
              draggable: false,
              infoWindow: InfoWindow(title: point.name.toString()),
              onTap: () {
                _showWorkerPanel(point.name, point.lastName1, point.lastName2, point.phonenumber,
                    point.location, point.rating);
              },
            ),
          );
        }
      }

      // Se obtiene la posicion actual del usuario y se actualiza la informacion de la camara
      cameraFollow(this._initLocation);

      // Condiciones usadas para la ejecucion del temporizador
      if (_start == 1) {
        _start = 1;
        _start++;
      } else {
        _start = 15;
      }
      if (!_timerON) {
        startTimer();
      }
    }

    return Stack(
      children: <Widget>[
        Scaffold(
          appBar: AppBar(title: Text("Showing Map")),
          drawer: MenuPage(),
          body: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,

            // Se espera a que el controlador cargue la informacion del mapa, una vez el mapa este listo
            // Se cambia el valor de {mapToggle} a *true* y se renderiza el mapa
            // Si el valor de {maptoggle} es *false* se muestra una pantalla de carga
            child: mapToggle
                ? GoogleMap(
                    onMapCreated: onMapCreated,
                    myLocationButtonEnabled: true,
                    myLocationEnabled: true,
                    compassEnabled: true,
                    initialCameraPosition: CameraPosition(
                        target: LatLng(this._initLocation.latitude ?? 0.0,
                            this._initLocation.longitude ?? 0.0),
                        zoom: 15),
                    markers: Set.from(markers),
                  )
                : Loading(),
          ),
        )
      ],
    );
  }

  // Funcion encargada del temporizador
  void startTimer() {
    _timerON = true;
    _markersON = false;
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) => setState(
        () {
          if (_start < 1) {
            timer.cancel();
            _timerON = false;
            _markersON = true;
          } else {
            _start = _start - 1;
          }
        },
      ),
    );
  }

  // En caso de que la aplicacion se cierre o el usuario se dirija a otra pantalla y el temporizador
  // Se encuentre en ejecucion se detiene el temporizador
  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  // Funcion dedicada a la obtencion de la posicion del usuario y actualizacion de la camara
  void cameraFollow(currentLocation) async {
    // Se obtiene la posicion actual del usuario
    Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high)
        .then((currloc) {
      if (currloc != null) {
        setState(() {
          _currentLocation = currloc;
        });
        // Se actualiza la camara reposicionandola en la nueva ubiciond el usuario
        CameraUpdate update = CameraUpdate.newCameraPosition(CameraPosition(
            target: LatLng(currloc.latitude ?? _currentLocation.latitude,
                currloc.longitude ?? _currentLocation.longitude),
            zoom: 16));
        mapController.animateCamera(update);
      }
    });
  }

  // Funcion dedicada al calculo de la distancia entre dos puntos geograficos
  int calculateDistance(double userLatitude, double userLongitude,
      double markerLatitude, double markerLongitude) {
    double latDistance = radians(userLatitude - markerLatitude);
    double lngDistance = radians(userLongitude - markerLongitude);

    double a = sin(latDistance / 2) * sin(latDistance / 2) +
        cos(radians(userLatitude)) *
            cos(radians(markerLatitude)) *
            sin(lngDistance / 2) *
            sin(lngDistance / 2);

    double c = 2 * atan2(sqrt(a), sqrt(1 - a));

    int distance = (AVERAGE_RADIUS_OF_EARTH_KM * c).toInt();

    return distance;
  }

  // Se espera a que el mapa sea creado y se asigna el controlador a una variable
  void onMapCreated(controller) {
    setState(() {
      mapController = controller;
    });
  }
}
