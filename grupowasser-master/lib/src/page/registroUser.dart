import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:grupo_wasser/src/css/estilos.dart';
import 'package:toast/toast.dart';

class Registro extends StatefulWidget {
  @override
  _RegistroState createState() => _RegistroState();
}

class _RegistroState extends State<Registro> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController nombreC = TextEditingController();
  TextEditingController app = TextEditingController();
  TextEditingController correo = TextEditingController();
  TextEditingController appm = TextEditingController();
  TextEditingController pass = TextEditingController();
  TextEditingController confirmPass = TextEditingController();
  bool mostrarContra = false;
  bool mostrarContra2 = false;
  int currentTipoUsuario;
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(17, 167, 174, 1),
        title: Text("Registro"),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Container(
            width: size.width * 0.90,
            child: Center(
              child: Form(
                key: _formKey,
                child: Center(
                  child: Column(
                    children: [
                      SizedBox(
                        height: 10.0,
                      ),
                      Text(
                        'Introduzca la información que se le solicita',
                        style: TextStyle(
                            color: Color.fromRGBO(96, 100, 98, 1),
                            fontWeight: FontWeight.w500,
                            fontSize: 18),
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      nombre(),
                      apellidoP(),
                      correoF(),
                      tipoUser(),
                      contrasena(),
                      contrasenaconfirm(),
                      botonRegistro(context),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget nombre() {
    return Container(
      margin: paddingTextFiel,
      child: TextFormField(
        validator: (value) {
          if (value.isEmpty) {
            return 'El campo es obligatorio';
          }
        },
        //textCapitalization: TextCapitalization.characters,
        keyboardType: TextInputType.text,
        controller: nombreC,
        style: textStileCampo,
        decoration: InputDecoration(
          filled: true,
          fillColor: fillColorCampo,
          border: borderCampo,
          enabledBorder: enabledBorderCampo,
          focusedBorder: focusedBorderCampo,
          contentPadding: contentPAddingCampo,
          hintText: 'Nombre(s)',
          hintStyle: campoTextoSstileHintText,
        ),
      ),
    );
  }

  Widget apellidoP() {
    return Container(
      margin: paddingTextFiel,
      child: TextFormField(
        validator: (value) {
          if (value.isEmpty) {
            return 'El campo es obligatorio';
          }
        },
        //textCapitalization: TextCapitalization.characters,
        keyboardType: TextInputType.text,
        controller: app,
        style: textStileCampo,
        decoration: InputDecoration(
          filled: true,
          fillColor: fillColorCampo,
          border: borderCampo,
          enabledBorder: enabledBorderCampo,
          focusedBorder: focusedBorderCampo,
          contentPadding: contentPAddingCampo,
          hintText: 'Primer apellido',
          hintStyle: campoTextoSstileHintText,
        ),
      ),
    );
  }

 
  Widget correoF() {
    return Container(
      margin: paddingTextFiel,
      child: TextFormField(
        validator: (value) {
          if (value.isEmpty) {
            return 'El campo es obligatorio';
          }
        },
        //textCapitalization: TextCapitalization.characters,
        keyboardType: TextInputType.emailAddress,
        controller: correo,
        style: textStileCampo,
        decoration: InputDecoration(
          filled: true,
          fillColor: fillColorCampo,
          border: borderCampo,
          enabledBorder: enabledBorderCampo,
          focusedBorder: focusedBorderCampo,
          contentPadding: contentPAddingCampo,
          hintText: 'Correo',
          hintStyle: campoTextoSstileHintText,
        ),
      ),
    );
  }

  Widget tipoUser() {
    return Container(
      margin: EdgeInsets.fromLTRB(5, 10, 5, 5),
      decoration: decorationSelect,
      child: DropdownButton<int>(
        underline: SizedBox(),
        isExpanded: true,
        items: [
          DropdownMenuItem<int>(
            child: Padding(
              padding: paddingSelectRegistro,
              child: Text(
                'Cliente',
                style: selectTexto,
              ),
            ),
            value: 0,
          ),
          DropdownMenuItem<int>(
            child: Padding(
              padding: paddingSelectRegistro,
              child: Text(
                'Alberquero',
                style: selectTexto,
              ),
            ),
            value: 1,
          ),
        ],
        hint: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.fromLTRB(12, 0, 12, 0),
              child: Text(
                'Tipo usuario',
                style: textoXDefectoSelect,
                textAlign: TextAlign.justify,
              ),
            )
          ],
        ),
        value: currentTipoUsuario,
        onChanged: (int values) {
          setState(() {
            currentTipoUsuario = values;
          });
        },
      ),
    );
  }

  Widget botonRegistro(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(5, 10, 5, 2),
      child: ButtonTheme(
        minWidth: double.infinity,
        child: RaisedButton(
          // padding: EdgeInsets.all(),
          onPressed: () {
            if (_formKey.currentState.validate()) {
              print("hola");
              if (currentTipoUsuario != null) {
                if (pass.text == confirmPass.text) {
                  guardarDatos();
                }
              } else {
                Toast.show('Seleccione el tipo de usuario', context,
                    duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
              }
            }
            //Navigator.popAndPushNamed(context, "/piscina");
          },
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(7.0)),
          elevation: 0,
          color: Color.fromRGBO(17, 167, 174, 1),
          textColor: Colors.white,
          child: Text(
            'Registrar',
            style: TextStyle(color: Colors.white),
          ),
        ),
      ),
    );
  }

  guardarDatos() async {
    try {
      UserCredential userCredential = await FirebaseAuth.instance
          .createUserWithEmailAndPassword(
              email: correo.text, password: pass.text);
      print("${userCredential.user}");
      if (userCredential != null) {
        guardarUsuario();
      }
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        Toast.show('La contraseña proporcionada es demasiado débil', context,
            duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
      } else if (e.code == 'email-already-in-use') {
        Toast.show('El correo que uso ya se encuentra ocupado', context,
            duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
      }
    } catch (e) {
      print(e.toString());
    }
  }

  guardarUsuario() {
    if (currentTipoUsuario == 0) {
      CollectionReference users =
          FirebaseFirestore.instance.collection('usaurios');
      users.doc("${correo.text}").set({
        "nombre": nombreC.text,
        "PrimerApellido": app.text,
        "tipo": currentTipoUsuario,
      }).then((value) {
        print("User Added");
        Navigator.pop(context);
      }).catchError((error) => print("Failed to add user: $error"));
    }
    if(currentTipoUsuario == 1){
       CollectionReference users =
          FirebaseFirestore.instance.collection('usaurios');
      users.doc("${correo.text}").set({
        "nombre": nombreC.text,
        "PrimerApellido": app.text,
        "tipo": currentTipoUsuario,
        "Availability":false,
        "Location":GeoPoint(0.0, 0.0),
      }).then((value) {
        print("User Added");
        Navigator.pop(context);
      }).catchError((error) => print("Failed to add user: $error"));
    }
  }

  Widget contrasena() {
    return Container(
      margin: paddingTextFiel,
      child: TextFormField(
        validator: (value) {
          if (value.isEmpty) {
            return 'El campo es obligatorio';
          }
        },
        keyboardType: TextInputType.text,
        controller: pass,
        obscureText: mostrarContra,
        style: textStileCampo,
        decoration: InputDecoration(
          filled: true,
          fillColor: fillColorCampo,
          border: borderCampo,
          enabledBorder: enabledBorderCampo,
          focusedBorder: focusedBorderCampo,
          contentPadding: contentPAddingCampo,
          hintText: 'Contraseña',
          hintStyle: campoTextoSstileHintText,
          suffixIcon: IconButton(
            icon: Icon(
              mostrarContra ? Icons.visibility_off : Icons.visibility,
              color: Color.fromRGBO(17, 167, 174, 1),
            ),
            onPressed: () {
              setState(() {
                mostrarContra = !mostrarContra;
              });
            },
          ),
        ),
      ),
    );
  }

  Widget contrasenaconfirm() {
    return Container(
      margin: paddingTextFiel,
      child: TextFormField(
        validator: (value) {
          if (value.isEmpty) {
            return 'El campo es obligatorio';
          }
        },
        keyboardType: TextInputType.text,
        controller: confirmPass,
        obscureText: mostrarContra2,
        style: textStileCampo,
        decoration: InputDecoration(
          filled: true,
          fillColor: fillColorCampo,
          border: borderCampo,
          enabledBorder: enabledBorderCampo,
          focusedBorder: focusedBorderCampo,
          contentPadding: contentPAddingCampo,
          hintText: 'Confirmar contraseña',
          hintStyle: campoTextoSstileHintText,
          suffixIcon: IconButton(
            icon: Icon(
              mostrarContra2 ? Icons.visibility_off : Icons.visibility,
              color: Color.fromRGBO(17, 167, 174, 1),
            ),
            onPressed: () {
              setState(() {
                mostrarContra2 = !mostrarContra2;
              });
            },
          ),
        ),
      ),
    );
  }
}
