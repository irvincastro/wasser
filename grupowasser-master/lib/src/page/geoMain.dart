/*
*     Vista que proporciona un stream de la informacion obtenida
*     desde firebase hacia la vista del mapa
*/

// ------------------------------|>| Importaciones |<|------------------------------
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'geopoint.dart';
import 'geoDatabase.dart';
import 'geoMap.dart';
import 'package:provider/provider.dart';

class GeoMain extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamProvider<List<Geopoint>>.value(
      value: GeoDatabaseService().markers,
      child: Scaffold(
        body: GeoMap(),
      ),
    );
  }
}
