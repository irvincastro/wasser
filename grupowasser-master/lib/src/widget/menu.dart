//Librerías para el módulo del menú
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:grupo_wasser/main.dart';

class MenuPage extends StatefulWidget {
  @override
  _MenuPageState createState() => _MenuPageState();
}

class _MenuPageState extends State<MenuPage> {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Drawer(
      elevation: 10,
      child: Container(
       // color: Color.fromRGBO(17, 167, 174, 1),
        child: Column(
          children: [
            Container(
              color: Color.fromRGBO(17, 167, 174, 1),
              padding: EdgeInsets.fromLTRB(0, 60, 0, 0),
              height: size.height * .25,
              child: DrawerHeader(
                  child: ListTile(
                title: Text(
                  "${prefs.correo}",
                  style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 14),
                ),
                subtitle: Text(
                  "${prefs.nombreUsuario}",
                  style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 14),
                ),
                
                leading: Icon(FontAwesomeIcons.userCircle, color: Colors.white,size: 40,),
              )),
             
            ),
            prefs.tipo != 1? ListTile(
              contentPadding: EdgeInsets.fromLTRB(10, 0, 10, 0),
              leading: Icon(
                Icons.home,
                color:  Color.fromRGBO(15, 159, 166, 1),
              ),
              title: Text(
                "Inicio",
                style: TextStyle(color:  Color.fromRGBO(15, 159, 166, 1),),
              ),
              onTap: () {
                 Navigator.popAndPushNamed(context, '/inicio');
              },
            ):Container(),
             prefs.tipo != 1?ListTile(
              contentPadding: EdgeInsets.fromLTRB(10, 0, 10, 0),
              leading: Icon(
                Icons.pool,
                color:  Color.fromRGBO(15, 159, 166, 1),
              ),
              title: Text(
                "Piscina",
                style: TextStyle(color:  Color.fromRGBO(15, 159, 166, 1),),
              ),
              onTap: () {
                 Navigator.popAndPushNamed(context, '/piscina');
              },
            ):Container(),
             prefs.tipo != 1?ListTile(
              contentPadding: EdgeInsets.fromLTRB(10, 0, 10, 0),
              leading: Icon(
                Icons.pool,
                color:  Color.fromRGBO(15, 159, 166, 1),
              ),
              title: Text(
                "Buscar alberqueros",
                style: TextStyle(color:  Color.fromRGBO(15, 159, 166, 1),),
              ),
              onTap: () {
                
                Navigator.popAndPushNamed(context,'/buscarAlberqueros');
              },
            ):Container(),

            //SERVICIOS
            prefs.tipo != 1?ListTile(
              contentPadding: EdgeInsets.fromLTRB(10, 0, 10, 0),
              leading: Icon(
                Icons.list,
                color:  Color.fromRGBO(15, 159, 166, 1),
              ),
              title: Text(
                "Servicios",
                style: TextStyle(color:  Color.fromRGBO(15, 159, 166, 1),),
              ),
              onTap: () {
                
                Navigator.popAndPushNamed(context,'/servicios');
              },
            ):Container(),
            //SERVICIOS

            ListTile(
              contentPadding: EdgeInsets.fromLTRB(10, 0, 10, 0),
              leading: Icon(
                Icons.exit_to_app,
                color:  Color.fromRGBO(15, 159, 166, 1),
              ),
              title: Text(
                "Cerrar Sesion",
                style: TextStyle(color:  Color.fromRGBO(15, 159, 166, 1),),
              ),
              onTap: () {
                prefs.clear();
                Navigator.pop(context);
                Navigator.pushNamed(context, '/login');
              },
            ),
          ],
        ),
      ),
    );
  }
}
