import 'package:flutter/material.dart';

class Figura extends CustomPainter{
  @override
  void paint(Canvas canvas, Size size) {

        Paint paint = Paint();
        paint.color = Colors.black;

        final rect = Rect.fromLTRB(0, 0, size.width, size.height);
        canvas.drawRect(rect, paint);

    }
  
    @override
    bool shouldRepaint(CustomPainter oldDelegate)=> false;
  
}