import 'package:flutter/material.dart';

//scaffold background Color
Color scaffoldColor = Color.fromRGBO(244, 248, 249, 1);

//appBar color aplica en todas las pantallas
Color backgroundColor = Color.fromRGBO(255, 255, 255, 1);

//Color del icono del appbar solo aplica en las que se tiene un iconTheme
IconThemeData iconTheme = IconThemeData(color: Color.fromRGBO(77, 138, 228, 1));

//estilo de letra y color del appbar (texto)// tambien es el mismo para algunos labels
TextStyle textStyle = TextStyle(
    color: Color.fromRGBO(96, 100, 98, 1),
    fontWeight: FontWeight.w500,
    fontSize: 24);

//color de los iconos del appbar(no todos los modulos lo usan)
Color actions = Color.fromRGBO(77, 138, 228, 1);

//leadign estilo
Icon iconLeading = Icon(
  Icons.arrow_back_ios,
);
//color leading
Color leadingColor = Color.fromRGBO(77, 138, 228, 1);

//color fondo de los container principal aplica en todos los modulos
Color boxDecoration = Color.fromRGBO(244, 248, 249, 1);

//Color de los botones para cerrar el anuncio aplica en los modulos que tienen anuncios de pantalla grande
Color colorIcon = Colors.white;
Color fondoDelBoton = Colors.blue;

//botones esto aplica para algunos de los botones del sistema (Flatbutton)
BoxDecoration boxDecorationBotonCircular = BoxDecoration(
    color: Color.fromRGBO(255, 255, 255, 1),
    borderRadius: BorderRadius.circular(100),
    boxShadow: [
      BoxShadow(
        color: Colors.grey.withOpacity(1),
        blurRadius: 3.0,
        spreadRadius: 0,
        offset: Offset(
          0.0,
          0.0,
        ),
      )
    ]);
Color botonCircular = Colors.blueAccent;

//color polyline
Color polilyne = Color.fromRGBO(77, 138, 228, 1);

//color del Marcker

//estilo del borde de los RaisedButton
RoundedRectangleBorder shape =
    RoundedRectangleBorder(borderRadius: BorderRadius.circular(7.0));

//color de los RaisedButton el fondo
Color raisedButtonColor = Color.fromRGBO(77, 138, 228, 1);

//color de los Raisedbutton texto
Color textoRaisedButton = Colors.white;

//Style RaisedButton
TextStyle raisedButtonStyle = TextStyle(color: Colors.white);

//color floating actionButton
Color floatingActionButtonColorBackGround = Color.fromRGBO(77, 138, 228, 1);

//color de los select
BoxDecoration boxDecorationselect = BoxDecoration(
  color: Color.fromRGBO(255, 255, 255, 1),
  borderRadius: BorderRadius.circular(7),
  border: Border.all(color: Color.fromRGBO(220, 220, 220, 1)),
);

//estilo texto de los select
TextStyle estiloSelectTexto =
    TextStyle(fontWeight: FontWeight.w600, color: Colors.black38, fontSize: 16);

//texto que va en la pantalla cuando no se encuentran resultados estas se encuentran en viajes.dart, contactos.dart y en lugaresrecientes.dart
TextStyle sinDatos = TextStyle(
    fontWeight: FontWeight.w600,
    color: Color.fromRGBO(152, 160, 156, 1),
    fontSize: 20);

// estilo de los textfield que estan en contactos y viajes
//todo esto es del textfiel de busqueda
OutlineInputBorder bordeTextField = OutlineInputBorder(
    borderRadius: BorderRadius.circular(100),
    borderSide: BorderSide(color: Color.fromRGBO(96, 100, 98, 1)));

OutlineInputBorder enabledBorder = OutlineInputBorder(
  borderRadius: BorderRadius.circular(100),
  borderSide: BorderSide(color: Color.fromRGBO(240, 240, 240, 1)),
);

OutlineInputBorder focusedBorder = OutlineInputBorder(
  borderRadius: BorderRadius.circular(7),
  borderSide: BorderSide(color: Color.fromRGBO(210, 210, 210, 1)),
);

EdgeInsetsGeometry contentPadding = EdgeInsets.fromLTRB(12, 0, 12, 0);
//este es el boxdecoration del context principal en donde se encuantra el textfield de busqueda
BoxDecoration decoracionTextFielBusqueda = BoxDecoration(
  color: Colors.white,
);

TextStyle hintStyle = TextStyle(
  fontWeight: FontWeight.w600,
  color: Color.fromRGBO(152, 160, 156, 1),
  fontSize: 16,
);

IconButton prefixIcon = IconButton(
  icon: Icon(
    Icons.search,
    color: Color.fromRGBO(122, 130, 126, 1),
  ),
  onPressed: () {},
  iconSize: 30.0,
);

Color fillColor = Color.fromRGBO(255, 255, 255, 1);

//buttonSpeed colores y estilos
Color butonSpeedColor = Color.fromRGBO(255, 255, 255, 1);
IconThemeData iconThemeDataButtonSpeed =
    IconThemeData(size: 40, color: Colors.blueAccent);
Color overlayColor = Colors.black;
CircleBorder shapeButtonSpeed = CircleBorder();

//estilo de los campos de texto
EdgeInsetsGeometry paddingTextFiel = EdgeInsets.fromLTRB(5, 10, 5, 5);

TextStyle textStileCampo = TextStyle(
  fontWeight: FontWeight.w600,
  color: Color.fromRGBO(70, 70, 70, 1),
  fontSize: 16,
);

Color fillColorCampo = Color.fromRGBO(255, 255, 255, 1);

OutlineInputBorder borderCampo = OutlineInputBorder(
    borderRadius: BorderRadius.circular(7),
    borderSide: BorderSide(color: Color.fromRGBO(96, 100, 98, 1)));

OutlineInputBorder enabledBorderCampo = OutlineInputBorder(
  borderRadius: BorderRadius.circular(7),
  borderSide: BorderSide(color: Color.fromRGBO(220, 220, 220, 1)),
);

OutlineInputBorder focusedBorderCampo = OutlineInputBorder(
  borderRadius: BorderRadius.circular(7),
  borderSide: BorderSide(color: Color.fromRGBO(190, 190, 190, 1)),
);

EdgeInsets contentPAddingCampo = EdgeInsets.fromLTRB(12, 0, 12, 0);

TextStyle campoTextoSstileHintText = TextStyle(
  fontWeight: FontWeight.w600,
  color: Colors.black38,
  fontSize: 16,
);

//estilo select Registro

BoxDecoration decorationSelect = BoxDecoration(
  color: Color.fromRGBO(255, 255, 255, 1),
  borderRadius: BorderRadius.circular(7),
  border: Border.all(color: Color.fromRGBO(220, 220, 220, 1)),
);

EdgeInsetsGeometry paddingSelectRegistro = EdgeInsets.fromLTRB(12, 0, 12, 0);

TextStyle selectTexto = TextStyle(
  fontWeight: FontWeight.w600,
  color: Color.fromRGBO(70, 70, 70, 1),
  fontSize: 16,
);

TextStyle textoXDefectoSelect =
    TextStyle(fontWeight: FontWeight.w600, color: Colors.black38, fontSize: 16);

//otro select, los estilos son diferentes
EdgeInsetsGeometry margenSelectRegistro = EdgeInsets.fromLTRB(5, 10, 5, 5);
//textStyle contenido select
TextStyle styleContenidoSelect =
    TextStyle(fontWeight: FontWeight.w600, color: Colors.black87);
