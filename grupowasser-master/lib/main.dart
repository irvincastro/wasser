import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:grupo_wasser/src/page/alberquero.dart';
import 'package:grupo_wasser/src/page/datosPiscina.dart';
import 'package:grupo_wasser/src/page/geoMain.dart';
import 'package:grupo_wasser/src/page/inicio.dart';
import 'package:grupo_wasser/src/page/login.dart';
import 'package:grupo_wasser/src/page/registroUser.dart';
import 'package:grupo_wasser/src/page/worker.dart';
import 'package:grupo_wasser/src/page/servicios.dart';
import 'package:grupo_wasser/src/preferences/preferencias.dart';
final prefs = new PreferenciasUsuario();

void main() async{
  
 WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await prefs.initPrefs();
    
  runApp(MyApp());
}
class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String route = '/login';
  @override
  Widget build(BuildContext context) {
    if(prefs.correo != ""){
      if(prefs.tipo == 0){
        setState(() {
        route = '/inicio';
      });
      }else{
        setState(() {
          route ='/worker'; 
        });
      }
    }
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Grupo Wasser",
      theme: ThemeData(
          
          primarySwatch: Colors.blueGrey,
          appBarTheme: AppBarTheme(
            color: Color.fromRGBO(17, 167, 174, 1),
          ),
        ),
      initialRoute: route,
      routes: <String,WidgetBuilder>{
        '/login':(BuildContext context)=>LoginPage(),
        '/inicio':(BuildContext context)=>HomePage(),
        '/piscina':(BuildContext context)=>PiscinaPage(),
        '/limpiador':(BuildContext context) => Limpiador(),
        '/registro':(BuildContext context)=>Registro(),
        '/buscarAlberqueros':(BuildContext context)=>GeoMain(),
        '/worker':(BuildContext context) =>WorkerView(),
        '/servicios':(BuildContext context) =>Servicios(),
      },
    );
  }
}
